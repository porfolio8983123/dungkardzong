import React, { useState } from 'react';
import './font.scss';
import SignUpButton from '../svg/SignUpButton';
import BookNowButton from '../svg/BookNowButton';
import Snail from '../Img/snail.png';
import { FaRegArrowAltCircleLeft } from "react-icons/fa";

const SignUpPage = () => {
  const [formData, setFormData] = useState({
    name: '',
    CID: '',
    mobileNumber: '+975',
    email: '',
    organization: ''
  });

  const [bookingData, setBookingData] = useState({
    conference: '',
    date: '',
    startTime: '',
    endTime: '',
    participants: '',
    packageSelected: false
  });

  const [formIndex, setFormIndex] = useState(0);
  const [errors, setErrors] = useState({});
  const [bookingErrors, setBookingErrors] = useState({});
  const [popUpMessage, setPopUpMessage] = useState('');

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData({ ...formData, [name]: value });
  };

  const handleBookingChange = (e) => {
    const { name, value, type, checked } = e.target;
    setBookingData({
      ...bookingData,
      [name]: type === 'checkbox' ? checked : value
    });
  };

  const validateForm = () => {
    const newErrors = {};
    if (!formData.name) newErrors.name = 'Name is required';
    if (!formData.CID || formData.CID.length !== 11) newErrors.CID = 'CID must contain exactly 11 digits!';
    if (!formData.mobileNumber || !/^\+975(17|77)\d{6}$/.test(formData.mobileNumber)) {
      newErrors.mobileNumber = 'Mobile number must start with +975 and follow the pattern +97517XXXXXX or +97577XXXXXX with a total of 8 digits after +975';
    }
    if (!formData.email || !/\S+@\S+\.\S+/.test(formData.email)) newErrors.email = 'Email is invalid';
    if (!formData.organization) newErrors.organization = 'Organization is required';

    setErrors(newErrors);
    if (Object.keys(newErrors).length > 0) {
      setPopUpMessage(Object.values(newErrors).join('<br />'));
      return false;
    }
    return true;
  };

  const validateBookingForm = () => {
    const newBookingErrors = {};
    if (!bookingData.conference) newBookingErrors.conference = 'Conference room is required';
    if (!bookingData.date) newBookingErrors.date = 'Date is required';
    if (!bookingData.startTime) newBookingErrors.startTime = 'Start time is required';
    if (!bookingData.endTime) newBookingErrors.endTime = 'End time is required';
    if (!bookingData.participants || bookingData.participants <= 0) newBookingErrors.participants = 'Number of participants is required';

    setBookingErrors(newBookingErrors);
    if (Object.keys(newBookingErrors).length > 0) {
      setPopUpMessage(Object.values(newBookingErrors).join('<br />'));
      return false;
    }
    return true;
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    if (validateForm()) {
      setFormIndex(1);
      setPopUpMessage('');
    }
  };

  const handleBookingSubmit = async (e) => {
    e.preventDefault();
    if (validateBookingForm()) {
      const userData = {
        CID: formData.CID,
        name: formData.name,
        phone: formData.mobileNumber,
        email: formData.email,
        organization: formData.organization
      }

      const conferenceData = {
        name: bookingData.conference
      }

      const meetingData = {
        date: bookingData.date,
        startingTime: bookingData.startTime,
        endingTime: bookingData.endTime,
        numberOfParticipants: parseInt(bookingData.participants),
        package: bookingData.packageSelected
      }

      const data = {
        userData,
        conferenceData,
        meetingData
      }

      await fetch("https://dungkarserver.onrender.com/api/v1/booking/bookConference",{
        method: "POST",
        headers: {
          "Content-Type":"application/json"
        },
        body: JSON.stringify(data)
      })
      .then( async (response) => {
        const responseData = await response.json();
        console.log("response ",responseData);

        if (responseData.status === 'success') {
          setFormIndex(2);
          setPopUpMessage('');
        }
      })
      .catch((error) => {
        console.log("error ",error);
      })
    }
  };

  const inputStyle = (fieldName, isBooking = false) => ({
    border: `1px solid ${isBooking ? (bookingErrors[fieldName] ? 'red' : bookingData[fieldName] ? '#FE3600' : 'black') : (errors[fieldName] ? 'red' : formData[fieldName] ? '#FE3600' : 'black')}`,
    color: `${isBooking ? (bookingData[fieldName] ? '#FE3600' : 'black') : (formData[fieldName] ? '#FE3600' : 'black')}`,
    padding: '10px',
    marginBottom: '8px',
    width: '112%',
    fontSize: '11px',
    fontFamily: 'Bricolage Grotesque',
    appearance: 'none',
  });

  const PartStyle = (fieldName, isBooking = false) => ({
    border: `1px solid ${isBooking ? (bookingData[fieldName] ? '#FE3600' : 'black') : (formData[fieldName] ? '#FE3600' : 'black')}`,
    color: `${isBooking ? (bookingData[fieldName] ? '#FE3600' : 'black') : (formData[fieldName] ? '#FE3600' : 'black')}`,
    padding: '10px',
    marginBottom: '8px',
    width: '105%',
    height: '36px',
    fontSize: '11px',
    fontFamily: 'Bricolage Grotesque',
    appearance: 'none',
  });

  const containerStyle = {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    minHeight: '50vh',
  };

  const formStyle = {
    maxWidth: '242px',
    paddingRight: '20px',
    marginLeft: '-10px',
  };

  const formContainerStyle = {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    width: '240px',
    marginLeft: '-10px',
  };

  const selectStyle = (fieldName, isBooking = false) => ({
    border: `1px solid ${isBooking ? (bookingErrors[fieldName] ? 'red' : bookingData[fieldName] ? '#FE3600' : 'black') : (formData[fieldName] ? '#FE3600' : 'black')}`,
    color: `${isBooking ? (bookingData[fieldName] ? '#FE3600' : 'black') : (formData[fieldName] ? '#FE3600' : 'black')}`,
    padding: '12px',
    height: '36px',
    marginBottom: '8px',
    width: '105%',
    fontSize: '11px',
    fontFamily: 'Bricolage Grotesque',
    appearance: 'none',
  });

  const popupContainerStyle = {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    width: '800px',
    height: '350px',
    padding: '20px',
    backgroundColor: 'white',
    boxShadow: '0px 0px 10px rgba(0,0,0,0.1)',
    textAlign: 'center',
    position: 'fixed',
    top: '35%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    zIndex: '1000',
  };

  const popUpMessageStyle = {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    width: '400px',
    padding: '20px',
    backgroundColor: 'white',
    boxShadow: '0px 0px 10px rgba(0,0,0,0.1)',
    textAlign: 'center',
    position: 'fixed',
    top: '20%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    zIndex: '1000',
  };

  return (
    <div style={containerStyle}>
      {popUpMessage && (
        <div style={popUpMessageStyle}>
          <p style={{ color: 'red', fontSize: '14px', marginBottom: '16px' }} dangerouslySetInnerHTML={{ __html: popUpMessage }}></p>
          <button onClick={() => setPopUpMessage('')} style={{ background: 'none', color: '#FE3600', fontSize: "15px", fontWeight: 'bold', padding: '10px 20px', border: 'none', cursor: 'pointer', marginTop: '10px' }}>
            Close
          </button>
        </div>
      )}
      {formIndex === 0 && (
        <h1 style={{ fontSize: '18px', marginBottom: '16px', fontWeight: '-moz-initial', letterSpacing: '2px', fontFamily: 'Bricolage Grotesque', color: 'white', paddingRight:'8px',display:'flex', alignItems:"center", width:"100%" }}>
          <p style={{textAlign:"center",width:"100%"}}>Booking</p>
        </h1>
      )}
      {formIndex === 1 && (
        <h1 style={{ fontSize: '18px', marginBottom: '16px', fontWeight: '-moz-initial', letterSpacing: '2px', fontFamily: 'Bricolage Grotesque', color: 'white', paddingRight:'10px',display:'flex', alignItems:"center", width:"100%" }}>
          <FaRegArrowAltCircleLeft size={15} style={{cursor:"pointer"}}
            onClick={() => {
              setFormIndex(0)
            }}
          />
          <p style={{textAlign:"center",width:"100%"}}>Booking</p>
        </h1>
      )}

      {formIndex === 0 &&
        <form style={formStyle} onSubmit={handleSubmit}>
          <input
            type="text"
            name="name"
            placeholder="Name"
            value={formData.name}
            onChange={handleChange}
            style={inputStyle("name")}
          />
          <input
            type="text"
            name="CID"
            placeholder="CID"
            value={formData.CID}
            onChange={handleChange}
            style={inputStyle("CID")}
          />
          <input
            type="text"
            name="mobileNumber"
            placeholder="Mobile Number (+97517XXXXXX or +97577XXXXXX)"
            value={formData.mobileNumber}
            onChange={handleChange}
            style={inputStyle("mobileNumber")}
          />
          <input
            type="email"
            name="email"
            placeholder="Email"
            value={formData.email}
            onChange={handleChange}
            style={inputStyle("email")}
          />
          <input
            type="text"
            name="organization"
            placeholder="Organization"
            value={formData.organization}
            onChange={handleChange}
            style={inputStyle("organization")}
          />
          <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', width: '260px' }}>
            <button
              type="submit"
              style={{ background: 'none', border: 'none', paddingRight: '10px', outline: 'none' }}
            ><SignUpButton /></button>
          </div>
        </form>
      }

      {formIndex === 1 &&
        <form style={formContainerStyle} onSubmit={handleBookingSubmit}>
          <select name="conference" value={bookingData.conference} onChange={handleBookingChange} style={selectStyle("conference", true)}>
            <option value="" disabled>Choose a room</option>
            <option value="Conference1">Meditation Center</option>
            <option value="Conference2">High-Altitude Athletics Training</option>
            <option value="Conference3">Seminar Hall 1</option>
            <option value="Conference4">Seminar Hall 2</option>
            <option value="Conference5">Seminar Hall 3</option>
            <option value="Conference6">Conference Hall</option>
            <option value="Conference7">Board Room</option>
            <option value="Conference8">Library</option>
            <option value="Conference9">Founders Court</option>
            <option value="Conference10">Multi-Purpose Hall</option>
            <option value="Conference11">Lounge Room</option>
            <option value="Conference12">Bilateral Room</option>
            <option value="Conference13">Dining Hall</option>
          </select>
          <input
            type="date"
            name="date"
            value={bookingData.date}
            placeholder='Select Date'
            onChange={handleBookingChange}
            style={{ ...selectStyle("date", true), borderColor: '#FE3600' }}
          />
          <input
            type="time"
            name="startTime"
            placeholder="Start Time"
            value={bookingData.startTime}
            onChange={handleBookingChange}
            style={{ ...selectStyle("startTime", true), borderColor: '#FE3600' }}
          />
          <input
            type="time"
            name="endTime"
            placeholder="End Time"
            value={bookingData.endTime}
            onChange={handleBookingChange}
            style={{ ...selectStyle("endTime", true), borderColor: '#FE3600' }}
          />
          <input
            type="number"
            name="participants"
            placeholder="Number of Participants"
            value={bookingData.participants}
            onChange={handleBookingChange}
            style={PartStyle("participants", true)}
          />
          <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', width: '275px', marginBottom: '8px' }}>
            <label style={{ fontFamily: 'Bricolage Grotesque', fontSize: '14px', color: 'white', display: 'flex', alignItems: 'center' }}>
              <input
                type="checkbox"
                name="packageSelected"
                checked={bookingData.packageSelected}
                onChange={handleBookingChange}
                style={{ marginRight: '8px', accentColor: 'white' }}
              />
              Select Package
            </label>
          </div>
          <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', width: '275px' }}>
            <button
              type="submit"
              style={{ background: 'none', border: 'none', outline: 'none' }}
            ><BookNowButton /></button>
          </div>
        </form>
      }

      {formIndex === 2 && (
        <div style={popupContainerStyle}>
          <div style={{ paddingTop: '10px', paddingBottom: "25px" }}>
            <img src={Snail} alt="Logo" className="poImg" />
          </div>
          <h2 style={{ fontFamily: 'Bricolage Grotesque', letterSpacing: '0.5px', fontSize: "25px", color: '#FE3600', fontWeight: 'bold' }}>Successfully Booked</h2>
          <p style={{ fontFamily: 'Bricolage Grotesque', letterSpacing: '0.5px', fontSize: "15px", fontWeight: 'bold', marginTop: "5px" }}>A representative from Dungkar Dzong <br />will reach out to you regarding the payment <br /> within 1-2 business days</p>
          <button onClick={() => setFormIndex(0)} style={{ background: 'none', color: '#FE3600', fontSize: "15px", fontWeight: 'bold', padding: '10px 20px', border: 'none', cursor: 'pointer', marginTop: '10px' }}>
            Close
          </button>
        </div>
      )}
    </div>
  );
};

export default SignUpPage;
