import React, { useState, useEffect } from 'react';
import { motion } from 'framer-motion';
import SignUpPage from './SignUpPage';
import backgroundImage from '../Img/back.png';

function SquareAnimation() {
  const [animate, setAnimate] = useState(false);
  const [showCenteredSquare, setShowCenteredSquare] = useState(false);
  const [triggered, setTriggered] = useState(false);

  useEffect(() => {
    const handleScroll = () => {
      const scrollPosition = window.scrollY + window.innerHeight;
      const threshold = document.getElementById('booking').offsetTop;

      if (scrollPosition >= threshold && !triggered) {
        setAnimate(true);
        setTriggered(true);
        setTimeout(() => {
          setShowCenteredSquare(true);
        }, 3000);
      }
    };

    window.addEventListener('scroll', handleScroll);

    return () => {
      window.removeEventListener('scroll', handleScroll);
    };
  }, [triggered]);

  return (
    <div 
      style={{ position: 'relative', width: '100%', minHeight: '74vh', display: 'flex', justifyContent: 'center', alignItems: 'center', backgroundImage: `linear-gradient(rgba(70, 45, 38, 0.41), rgba(70, 45, 38, 0.41)), url(${backgroundImage})`, backgroundSize: 'cover', backgroundPosition: 'middle', overflow:'hidden', zIndex: 999}}
      id="booking"
    >
      <motion.div
        style={{ width: '100px', height: '100px', position: 'absolute', opacity: animate ? 0 : 1, transition: 'opacity 3s' }}
      >
        <svg viewBox="0 0 100 100" style={{ width: '100px', height: '100px' }}>
          <rect x="5" y="5" width="90" height="90" stroke="white" strokeWidth="0.5" fill="none" />
        </svg>
      </motion.div>
      <motion.div
        animate={{ rotate: animate ? 45 : 0 }}
        transition={{ duration: 1 }}
        style={{ width: '100px', height: '100px', position: 'absolute', opacity: animate ? 0 : 1, transition: 'opacity 3s' }}
      >
        <svg viewBox="0 0 100 100" style={{ width: '100px', height: '100px' }}>
          <rect x="5" y="5" width="90" height="90" stroke="white" strokeWidth="0.5" fill="none" />
        </svg>
      </motion.div>
      {animate && (
        <motion.div
          animate={{ scale: animate ? 5 : 1 }}
          transition={{ duration: 1, delay: 1.5 }} 
          style={{ width: '100px', height: '100px', position: 'absolute', opacity: animate ? 1 : 0, transition: 'opacity 3s' }}
        >
          <svg viewBox="0 0 100 100" style={{ width: '100px', height: '100px' }}>
            <rect x="5" y="5" width="90" height="90" stroke="white" strokeWidth="0.1" fill="none" />
          </svg>
        </motion.div>
      )}
      <motion.div
        style={{ position: 'absolute', opacity: showCenteredSquare ? 1 : 0, transition: 'opacity 1s' }}
      >
        <svg viewBox="-50 -50 100 100" style={{ width: '850px', height: '850px', transform: 'rotate(45deg)', transformOrigin:'center' }}>
          <rect x="-25" y="-25" width="50" height="50" stroke="white" strokeWidth="0.08" fill="none"/>
        </svg>
      </motion.div>
      <motion.div
        style={{ position: 'absolute', opacity: showCenteredSquare ? 1 : 0, transition: 'opacity 1s' }}
      >
        <svg viewBox="-50 -50 100 100" style={{ width: '1800px', height: '2000px', transform: 'rotate(45deg)', transformOrigin:'center' }}>
          <rect x="-25" y="-25" width="50" height="50" stroke="white" strokeWidth="0.05" fill="none"/>
        </svg>
      </motion.div>
      <motion.div
        style={{ position: 'absolute', opacity: showCenteredSquare ? 1 : 0, transition: 'opacity 1s' }}
      >
        <SignUpPage />
      </motion.div>
    </div>
  );
}

export default SquareAnimation;
