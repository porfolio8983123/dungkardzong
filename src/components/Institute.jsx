import React from 'react';
import '../Styles/institute.scss';
import Readmore from './svg/Readmore';
import { motion } from 'framer-motion';

const Institute = () => {
  return (
    <div className='institute__container' style={{zIndex:"999",backgroundColor:"#FFCE00"}}>
        <motion.div
            initial = {{y: 50, opacity: 0}}
            whileInView={{ y: 0, opacity: 1 }}
            transition={{delay:0.4,duration:1}}
        className='title' style={{color:"black"}}>
            The Druk Gyalpo's Institute
        </motion.div>
        <motion.div 
            initial = {{y: 40, opacity: 0}}
            whileInView={{ y: 0, opacity: 1 }}
            transition={{delay:0.8,duration:1}}
        className='description' style={{color:"black",textAlign:"center"}}>
            Good citizenship is key to creating our children’s tomorrow and quality education is a fundamental pillar for good citizenship. This is even more crucial in a world that is rapidly changing and where human movement and migration are occurring at a scale never seen before. Realizing the need for novel skills and new mindsets to face these new challenges, His Majesty envisioned a unique model of education that not only focuses on the cerebral and physical growth of an individual but also on an individual’s emotional, social and spiritual growth.
        </motion.div>
        <motion.div
            initial = {{y: 50, opacity: 0}}
            whileInView={{ y: 0, opacity: 1 }}
            transition={{delay:1.2,duration:1}}
        className='institute__button'>
           <a href="https://motherboard.dgi.edu.bt/" target='_blank'>
                <button className='svg__button'>
                    <Readmore color = "black"/>
                </button>
           </a>
        </motion.div>
    </div>
  )
}

export default Institute