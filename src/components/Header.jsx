import React, { useState } from 'react';
import '../Styles/header.scss';
import logo from '../assets/logo/logo.png';
import { MdOutlineMenu } from "react-icons/md";
import { IoCloseSharp } from "react-icons/io5";
import { motion } from 'framer-motion';
import { useNavigate, useLocation } from 'react-router-dom';
import gesarling from '../../public/geaserling.png';

const Header = () => {
  const navigate = useNavigate();
  const location = useLocation();
  const [menuOpen, setMenuOpen] = useState(false);

  const handleNavigation = (path) => {
    setMenuOpen(false);
    navigate(path);
  };

  const getActiveClass = (path) => {
    return location.pathname === path ? 'active' : '';
  };

  return (
    <div className='header__container'>
      <div className='logo__container'>
        <img src={gesarling} alt="Logo" width={50} />
        <div className='header__logo__title__container'>
          <p className='header__dungkar'>Gesarling</p>
          {/* <p className='header__sub'>Bhutan Royal Academy</p> */}
        </div>
      </div>

      {!menuOpen && (
        <motion.div
          className='small__navigation'
          initial={{ y: -10, opacity: 0 }}
          animate={{ y: 0, opacity: 1 }}
          transition={{ delay: 0.1, duration: 0.5 }}
        >
          <motion.div
            className='small_navigation__container'
            initial={{ y: -10, opacity: 0 }}
            animate={{ y: 0, opacity: 1 }}
            transition={{ delay: 0.2, duration: 0.5 }}
          >
            <div className={`header__nav ${getActiveClass('/home')}`} onClick={() => handleNavigation('/home')}>
              Home
            </div>
            <div className={`header__nav ${getActiveClass('/ourStory')}`} onClick={() => handleNavigation('/ourStory')}>
              Vision
            </div>
            <div className={`header__nav ${getActiveClass('/news')}`} onClick={() => handleNavigation('/news')}>
              Events
            </div>
            <div className={`header__nav ${getActiveClass('/facilities')}`} onClick={() => handleNavigation('/facilities')}>
              Facilities
            </div>
          </motion.div>

          <motion.div
            className='hamburger__menu'
            initial={{ y: -10, opacity: 0 }}
            animate={{ y: 0, opacity: 1 }}
            transition={{ delay: 0.1, duration: 0.5 }}
          >
            <IoCloseSharp size={32} color='white' onClick={() => setMenuOpen(!menuOpen)} />
          </motion.div>
        </motion.div>
      )}

      {menuOpen && (
        <motion.div
          className='hamburger__menu'
          initial={{ y: -10, opacity: 0 }}
          animate={{ y: 0, opacity: 1 }}
          transition={{ delay: 0.1, duration: 0.5 }}
        >
          <MdOutlineMenu size={32} color='white' onClick={() => setMenuOpen(!menuOpen)} />
        </motion.div>
      )}

      <div className='header__navigation__container'>
        <div className={`header__nav ${getActiveClass('/home')}`} onClick={() => handleNavigation('/')}>
          Home
        </div>
        <div className={`header__nav ${getActiveClass('/ourStory')}`} onClick={() => handleNavigation('/ourStory')}>
          Vision
        </div>
        <div className={`header__nav ${getActiveClass('/news')}`} onClick={() => handleNavigation('/news')}>
          Events
        </div>
        <div className={`header__nav ${getActiveClass('/facilities')}`} onClick={() => handleNavigation('/facilities')}>
          Facilities
        </div>
      </div>
    </div>
  );
};

export default Header;
