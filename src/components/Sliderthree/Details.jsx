import React from 'react';
import { Container, Row, Col, Tab } from 'react-bootstrap';
import { ProjectCard } from './DetailsCard';
import projImg1 from '../Img/img1.jpeg';
import projImg2 from '../Img/img2.jpeg';
import projImg3 from '../Img/img3.jpg';
import projImg4 from '../Img/img4.jpg';
import projImg5 from '../Img/img5.png';
import projImg6 from '../Img/img6.png';

import './Details.scss';
import TrackVisibility from 'react-on-screen';

export const Projects = () => {

  const projects = [
    {
      title: "Panoramic view of Pangbisa village",
      description: "Pangbisa, nestled at 2500-3000 meters above sea level, is a serene village just a half-hour drive from Paro International Airport. With approximately 25 households, this village is renowned for its historical and spiritual significance.",
      imgUrl: projImg1,
    },
    {
      title: "Ugyen Guru Lhakhang",
      description: "The sacred Ugyen Guru Lhakhang crowns the ridge, overlooking the valley. This ancient temple, founded by Terton Sherab Mebar, houses sacred relics and stands as a testament to the village's deep spiritual roots.",
      imgUrl: projImg2,
    },
    {
      title: "Terton Sherab Mebar",
      description: "Terton Sherab Mebar (1267-1326), the revered treasure revealer, founded Ugyen Guru Lhakhang. His expeditions across Bhutan left a legacy of sacred relics and spiritual teachings.",
      imgUrl: projImg3,
    },
    {
      title: "Legends and Myths",
      description: `Legends say Zhabdrung Ngawang Namgyel visited Pangbisa in the 16th century, seeking followers. Unwelcomed by the devoted locals, he named it "Abandoned Land" and moved to the neighboring village.`,
      imgUrl: projImg4,
    },
    {
      title: "Spiritual Guardians",
      description: "Protected by the Four Tsens and blessed by the Five Long Life Sisters, Pangbisa is a spiritual sanctuary. These guardians and deities are believed to watch over the village, ensuring its prosperity and spiritual harmony.",
      imgUrl: projImg5,
    },
    {
      title: "The Druk Gyalpo’s Institute",
      description: "His Majesty Jigme Khesar Namgyel Wangchuck envisioned The Druk Gyalpo’s Institute as a beacon of holistic education. Spanning 285 acres, the Institute bridges ancient wisdom with modern educational practices.",
      imgUrl: projImg6,
    },
  ];

  return (
    <section className="project" id="projects">
      <Container>
        <Row>
          <Col size={12}>
            <TrackVisibility>
              {({ isVisible }) =>
                <div className={isVisible ? "animate__animated animate__fadeIn" : ""}>
                  <h2>Stories</h2>
                  <p>Every great achievement begins with an auspicious moment, where the seeds of success are sown with hope and vision.</p>
                  <Tab.Container id="projects-tabs" defaultActiveKey="first">
                    <Tab.Content id="slideInUp" className={isVisible ? "animate__animated animate__slideInUp" : ""}>
                      <Tab.Pane eventKey="first">
                        <Row className="projects-row">
                          {projects.map((project, index) => (
                            <Col key={index} className="project-card">
                              <ProjectCard {...project} />
                            </Col>
                          ))}
                        </Row>
                      </Tab.Pane>
                    </Tab.Content>
                  </Tab.Container>
                </div>
              }
            </TrackVisibility>
          </Col>
        </Row>
      </Container>
    </section>
  );
};
