import React from 'react'
import Header from './Header'
import Footer from './Footer'
import '../Styles/newsEventDetails.scss';

const NewsEventDetails = () => {
  return (
    <div className='newsEvent__container'>
        <Header />

        <div className='newsEvent__innercontainer'>
            <div className='header__title'>
                News & Events
            </div>
        </div>

        <Footer />
    </div>
  )
}

export default NewsEventDetails