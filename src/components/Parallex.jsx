import { motion, useScroll, useTransform} from "framer-motion";
import { useRef, useState, useEffect } from "react";
import { useMediaQuery } from 'react-responsive';

import LoadingAnimation from "./LoadingAnimation";
import "../Styles/Parallex.scss";

import Sky from '../assets/sky.webp';
import Wintersky from '../assets/winter/sky.webp';
import Springsky from '../assets/spring/sky.webp';
import Summersky from '../assets/summer/sky.webp';
import Autumnsky from '../assets/autumn/sky.webp'; 

import mountainBG from '../assets/bgmountain.svg';
import WintermountainBG from '../assets/winter/bgmountain.webp';
import SpringmountainBG from '../assets/spring/bgmountain.webp'; 
import SummermountainBG from '../assets/summer/bgmountain.svg';
import AutumnmountainBG from '../assets/autumn/bgmountain.svg';

import mountainFG from '../assets/mountainFG.svg';
import WintermountainFG from '../assets/winter/mountainFG.svg';
import SpringmountainFG from '../assets/spring/mountainFG.svg';
import SummermountainFG from '../assets/summer/mountainFG.svg';
import AutumnmountainFG from '../assets/autumn/mountainFG.svg';

import mountainFG2 from '../assets/mountaifg2.svg';
import WintermountainFG2 from '../assets/winter/mountaifg2.svg';
import SpringmountainFG2 from '../assets/spring/mountaifg2.svg';
import SummermountainFG2 from '../assets/summer/mountaifg2.svg';
import AutumnmountainFG2 from '../assets/autumn/mountaifg2.svg';

import Dzong from '../assets/MainFG.webp';
import Winterdzong from '../assets/winter/MainFG.webp';
import Springdzong from '../assets/spring/MainFG.webp';
import Summerdzong from '../assets/summer/MainFG.webp';
import Autumndzong from '../assets/autumn/MainFG.webp';

import WinterSmall from "./svg/WinterSmall";
import SpringSmall from "./svg/SpringSmall";
import SummerSmall from "./svg/SummerSmall";
import AutumnSmall from "./svg/AutumnSmall";

import winterBig from '../assets/winter/winterBig.svg'
import springBig from '../assets/spring/springBig.svg'
import summerBig from '../assets/summer/summerBig.svg'
import autumnBig from '../assets/autumn/autumnBig.svg'

import logo from "../assets/logo.svg";
import SunMoon from "../assets/sun&moon.svg"

export default function Parallax() {
  const ref = useRef(null);
  const titleRef = useRef(null);
  const buttonDivRef = useRef(null);
  const { scrollYProgress } = useScroll({
    target: ref,
    offset: ["start start", "end end"],
  });

  const isMobile = useMediaQuery({ query: '(max-width: 500px)' });
  const isTablet = useMediaQuery({ query: '(min-width: 601px) and (max-width: 1024px)' });
  const isDesktop = useMediaQuery({ query: '(min-width: 1025px)' });

  const skyY = useTransform(
    scrollYProgress,
    isMobile ? [0, 0.1] : isTablet ? [0, 1.5] : [0, 1],
    isMobile ? ["-10%", "0%"] : isTablet ? ["0%", "0%"] : ["-20%", "0%"]
  );
  const dzongY = useTransform(
    scrollYProgress,
    isMobile ? [0, 0.1] : isTablet ? [0, 1] : [0, 0.8],
    isMobile ? ["290%", "220%"] : isTablet ? ["50%", "25%"] : ["30%", "10%"]
  );
  const mountainY = useTransform(
    scrollYProgress,
    isMobile ? [0, 0.1] : isTablet ? [0, 1.5] : [0, 1.5],
    isMobile ? ["50%", "-60%"] : isTablet ? ["170%", "70%"] : ["40%", "-20%"]
  );
  const leftMountainX = useTransform(
    scrollYProgress,
    isMobile ? [0, 0.1] : isTablet ? [0, 1.5] : [0, 1],
    isMobile ? ["-70%", "0%"] : isTablet ? ["-95%", "40%"] : ["-95%", "0%"]
  );
  const leftMountainY = useTransform(
    scrollYProgress,
    isMobile ? [0, 0.1] : isTablet ? [0, 1.5] : [0, 1],
    isMobile ? ["0%", "90%"] : isTablet ? ["-95%", "150%"] : ["40%", "0%"]
  );
  const rightMountainX = useTransform(
    scrollYProgress,
    isMobile ? [0, 0.1] : isTablet ? [0, 1] : [0, 1],
    isMobile ? ["25%", "-40%"] : isTablet ? ["40%", "0%"] : ["30%", "0%"]
  );

  const labels = useTransform(
    scrollYProgress,
    isMobile ? [0, 0.1] : isTablet ? [0, 1] : [0, 1],
    isMobile ? ["10%", "15%"] : isTablet ? ["0%", "20%"] : ["5%", "40%"]
  );

  const[imagesLoaded,setImagesLoaded] = useState(false)
  const [imagesDisplaced, setImagesDisplaced] = useState(false);
  const [skyImage, setSkyImage] = useState(Sky);
  const [mbgImage, setMbgImage] = useState(mountainBG);
  const [FG1Image, setFG1Image] = useState(mountainFG);
  const [FG2Image, setFG2Image] = useState(mountainFG2);
  const [dzongImage, setDzongImage] = useState(Dzong);
  const [seasonText, setSeasonText] = useState("Our Heavenly");
  const [monthText, setMonthText] = useState(null);
  const [tempText, setTempText] = useState(null);
  const [lowestTempText, setLowestTempText] = useState(null);
  const [lowestTemp, setLowestTemp] = useState(null);
  const [precipitation,setPrecipitation] = useState(null);
  const [humidity, setHumidity] = useState(null);
  const [wind,setWind] = useState(null);
  const [precipitationText, setPrecipitationText] = useState(null);
  const [humidityText, setHumidityText] = useState(null);
  const [windText, setWindText] = useState(null);
  const [sunmoon,setSunMoon] = useState(null);
  const [sun,setSun] = useState(null);
  const [moon,setMoon] = useState(null);
  const [sunriseText, setSunRiseText] = useState(null);
  const [sunsetText, setSunSetText] = useState(null);
  const [moonriseText, setMoonRiseText] = useState(null);
  const [moonsetText, setMoonSetText] = useState(null);
  const [seasonBig, setSeasonBig] = useState(null);

  const [cardColor, setCardColor] = useState(null);
  const [textColor, setTextColor] = useState("#FFFFFF");
  const [activeButton, setActiveButton] = useState(null);


  const handleImageChange = (season) => {
    setActiveButton(season);
    switch (season) {
      case "Winter":
        setSkyImage(Wintersky);
        setMbgImage(WintermountainBG);
        setFG1Image(WintermountainFG);
        setFG2Image(WintermountainFG2);
        setDzongImage(Winterdzong);
        setSeasonText("Winter Time");
        setTextColor("#5ABAFF"); // Blue for Winter
        setMonthText("Dec-Feb");
        setTempText("-7℃");
        setLowestTemp("Lowest Temperature")
        setLowestTempText("8.5℃ Highest")
        setPrecipitation("Precipitation")
        setHumidity("Humidity")
        setWind("Wind")
        setPrecipitationText("16mm")
        setHumidityText("83%")
        setWindText("2-4km/hr")
        setSunMoon(SunMoon)
        setSun("SUN")
        setSunRiseText("Rise: 6:46 A.M")
        setSunSetText("Set: 17:31 P.M")
        setMoon("MOON")
        setMoonRiseText("Rise: 7:15 P.M")
        setMoonSetText("Set: 5:00 A.M")
        setCardColor("#5ABAFF")
        setSeasonBig(winterBig)
        break;
      case "Spring":
        setSkyImage(Springsky);
        setMbgImage(SpringmountainBG);
        setFG1Image(SpringmountainFG);
        setFG2Image(SpringmountainFG2);
        setDzongImage(Springdzong);
        setSeasonText("Spring Time");
        setTextColor("#06B300"); // Green for Spring
        setMonthText("March-May");
        setTempText("-2℃");
        setLowestTemp("Lowest Temperature")
        setLowestTempText("15℃ Highest")
        setPrecipitation("Precipitation")
        setHumidity("Humidity")
        setWind("Wind")
        setPrecipitationText("20-24%")
        setHumidityText("80-95%")
        setWindText("2-4km/hr")
        setSunMoon(SunMoon)
        setSun("SUN")
        setCardColor("#06B300")
        setSunRiseText("Rise: 5:15 A.M")
        setSunSetText("Set: 6:00 P.M")
        setMoon("MOON")
        setMoonRiseText("Rise: 7:15 P.M")
        setMoonSetText("Set: 5:00 A.M")
        setSeasonBig(springBig)
        break;
      case "Summer":
        setSkyImage(Summersky);
        setMbgImage(SummermountainBG);
        setFG1Image(SummermountainFG);
        setFG2Image(SummermountainFG2);
        setDzongImage(Summerdzong);
        setSeasonText("Summer Time");
        setTextColor("#FFB800"); // Yellow for Summer
        setMonthText("June-Aug");
        setTempText("11℃");
        setLowestTemp("Lowest Temperature")
        setLowestTempText("22℃ Highest")
        setPrecipitation("Precipitation")
        setHumidity("Humidity")
        setWind("Wind")
        setPrecipitationText("20mm")
        setHumidityText("90%")
        setWindText("2-4km/hr")
        setSunMoon(SunMoon)
        setSun("SUN")
        setCardColor("#FFB800")
        setSunSetText("Set: 6:00 P.M")
        setMoon("MOON")
        setMoonRiseText("Rise: 7:15 P.M")
        setMoonSetText("Set: 5:00 A.M")
        setSeasonBig(summerBig)
        break;
      case "Autumn":
        setSkyImage(Autumnsky);
        setMbgImage(AutumnmountainBG);
        setFG1Image(AutumnmountainFG);
        setFG2Image(AutumnmountainFG2);
        setDzongImage(Autumndzong);
        setSeasonText("Autumn Time");
        setTextColor("#E35E13"); // Orange for Autumn
        setMonthText("Sept-Nov");
        setTempText("3℃");
        setLowestTemp("Lowest Temperature")
        setLowestTempText("14℃ Highest")
        setPrecipitation("Precipitation")
        setHumidity("Humidity")
        setWind("Wind")
        setPrecipitationText("16mm")
        setHumidityText("83%")
        setWindText("2-4km/hr")
        setSunMoon(SunMoon)
        setSun("SUN")
        setCardColor("#E35E13")
        setSunSetText("Set: 6:00 P.M")
        setMoonRiseText("Rise: 7:15 P.M")
        setMoon("MOON")
        setMoonSetText("Set: 5:00 A.M")
        setSeasonBig(autumnBig)

        break;
      default:
        break;
    }
  };

  useEffect(() => {
    const handleScroll = () => {
      const sections = document.querySelectorAll(".main");
      let currentSectionIndex = -1;

      sections.forEach((section, index) => {
        const rect = section.getBoundingClientRect();
        if (rect.top <= window.innerHeight / 2 && rect.bottom >= window.innerHeight / 2) {
          currentSectionIndex = index;
        }
      });

      if (currentSectionIndex !== -1) {
        const nextSection = sections[currentSectionIndex + 1];
        if (nextSection) {
          nextSection.scrollIntoView({ behavior: "smooth" });
        }
      }
    };

    window.addEventListener("scroll", handleScroll);

    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  useEffect(() => {
    if (titleRef.current && buttonDivRef.current) {
      buttonDivRef.current.style.width = `${titleRef.current.offsetWidth}px`;
    }
  }, []);
  useEffect(() => {
    const preloadImages = async () => {
      const images = [
        Sky,
        Wintersky, 
        Springsky, 
        Summersky, 
        Autumnsky,
        Dzong,
        Winterdzong,
        Springdzong,
        Summerdzong, 
        Autumndzong, 
        mountainBG,
        WintermountainBG,
        SpringmountainBG,
        SummermountainBG,
        AutumnmountainBG,
        mountainFG,
        WintermountainFG,
        SummermountainFG,
        SpringmountainFG,
        AutumnmountainFG,
        mountainFG2,
        WintermountainFG2,
        SpringmountainFG2,
        SummermountainFG2,
        AutumnmountainFG2];
      await Promise.all(images.map((image) => new Promise((resolve) => {
        const img = new Image();
        img.src = image;
        img.onload = resolve;
      })));

      setImagesLoaded(true);
    };
  
    preloadImages();
  }, []);
  

  return (
    <div ref={ref}>
    {imagesLoaded && (
    <div className="main" >
      <motion.div className="Text" style={{ y: labels, transition: { duration: 1, ease: "easeInOut" }}}>
        <img src={logo} alt="Logo" className="logo" />
        <h1 className="subTitle" style={{ color: textColor }}>{seasonText}</h1>
        <h1 className="Title" ref={titleRef}>DUNGKAR DZONG</h1>
        <motion.div ref={buttonDivRef} className="buttonDiv" style={{ display: 'flex', justifyContent: 'space-around',width:"100% !important"}}>
          <motion.button 
          onClick={() => handleImageChange("Winter")}
          animate={{ backgroundColor: activeButton === "Winter" ? "#FFFFFF" : "transparent", color: activeButton === "Winter" ? textColor : "#FFFFFF" }}
          transition={{ duration: 0.5 }}><div style={{display:"flex"}}><div><WinterSmall color={activeButton === "Winter" ? textColor : "#FFFFFF"}/></div><div>Winter</div></div></motion.button>
          <motion.button 
          onClick={() => handleImageChange("Spring")} 
          animate={{ color: activeButton === "Spring" ? textColor : "#FFFFFF",backgroundColor: activeButton === "Spring" ? "#FFFFFF" : "transparent" }}
          transition={{ duration: 0.5 }}><div style={{display:"flex"}}><div><SpringSmall color={activeButton === "Spring" ? textColor : "#FFFFFF"}/></div><div>Spring</div></div></motion.button>
          <motion.button 
          onClick={() => handleImageChange("Summer")} 
          animate={{ color: activeButton === "Summer" ? textColor : "#FFFFFF",backgroundColor: activeButton === "Summer" ? "#FFFFFF" : "transparent" }}
          transition={{ duration: 0.5 }}><div style={{display:"flex"}}><div><SummerSmall color={activeButton === "Summer" ? textColor : "#FFFFFF"}/></div><div>Summer</div></div></motion.button>
          <motion.button 
          onClick={() => handleImageChange("Autumn")} 
          animate={{ color: activeButton === "Autumn" ? textColor : "#FFFFFF",backgroundColor: activeButton === "Autumn" ? "#FFFFFF" : "transparent" }}
          transition={{ duration: 0.5 }}><div style={{display:"flex"}}><div><AutumnSmall color={activeButton === "Autumn" ? textColor : "#FFFFFF"}/></div><div>Autumn</div></div></motion.button>
        </motion.div>

        <div className="cardDiv">
          <div className="card" style={{backgroundColor:cardColor,display:"flex",alignItems:"center"}}>
            <div className="content" style={{alignItems:"center"}}>
              <div><img src={seasonBig}></img></div>
              <div><h1>{monthText}</h1></div>
            </div>
          </div>
          <div className="card" style={{backgroundColor:cardColor,display:"flex",alignItems:"center"}}>
            <div className="content" style={{margin:"5%",alignItems:"flex-start"}}>
              <h1 style={{fontSize:isMobile ? '2.5rem' : '4rem',fontWeight:"800"}}>{tempText}</h1>
              <p>{lowestTemp}</p>
              <p>{lowestTempText}</p>
            </div>
            <div className="content" style={{display:"flex",flexDirection:"column",padding:isMobile ? '0.6em' : '0.8em'}}>
                <p className="bold">{precipitationText}</p>
                <p>{precipitation}</p>
                <p className="bold">{humidityText}</p>
                <p>{humidity}</p>
                <p className="bold">{windText}</p>
                <p>{wind}</p>
            </div>
          </div>
          <div className="card" style={{backgroundColor:cardColor,display:"flex"}}>
          <div className="content" style={{margin:"5%"}}>
            <div><img src={sunmoon} style={{width:isMobile ? '100%' : '120%'}}></img></div>

            </div>
            <div className="content" style={{height:"100%"}}>
            <div style={{padding:"0.8em",height:"100%"}}>

              <div style={{margin:"5px",height:"50%"}}>
              <p className="bold">{sun}</p>
              <p style={{whiteSpace:"nowrap"}}>{sunriseText}</p>
              <p>{sunsetText}</p>
              </div>

              <div style={{margin:"5px",height:"50%"}}>
              <p className="bold">{moon}</p>
              <p>{moonriseText}</p>
              <p>{moonsetText}</p>
              </div>

            </div>
            </div>
          </div>
        </div>
      </motion.div>
      <motion.div className="sky"
        style={{
          y: skyY,
          // backgroundImage: `url(${skyImage})`,
          transition: { duration: 0.5, ease: "linear" },
          position: imagesDisplaced ? "fixed" : "fixed",
        }}
      >
        <motion.img src={skyImage} style={{width:"100%",height: isMobile ? '80em': isTablet ? '80em' : '100%' , objectFit:"cover"}}/>
      </motion.div>
      <motion.div className="mountainFG"
        style={{
          x: leftMountainX,
          y: leftMountainY,
          // backgroundImage: `url(${FG1Image})`,
          transition: { duration: 0.5, ease: "linear" },
          position: imagesDisplaced ? "fixed" : "fixed",
        }}
      >
        <motion.img src={FG1Image} style={{width:isMobile ? '50%' : isTablet ? '60%' : '100%',objectFit:"cover"}}/>
        </motion.div>
      <motion.div className="mountainFG2"
        style={{
          x: rightMountainX,
          // backgroundImage: `url(${FG2Image})`,
          transition: { duration: 0.5, ease: "linear" },
          position: imagesDisplaced ? "fixed" : "fixed",
        }}
      >
        <motion.img src={FG2Image} style={{width:"100%"}}/>
        </motion.div>
      <motion.div className="mountainBG"
        style={{
          y: mountainY,
          // backgroundImage: `url(${mbgImage})`,
          transition: { duration: 0.5, ease: "linear" },
          position: imagesDisplaced ? "fixed" : "fixed",
        }}
      >
        <motion.img src={mbgImage} style={{width:isMobile ? '100%' : '100%',objectFit:"cover"}}/>

        </motion.div>
      <motion.div className="dzong"
        style={{
          y: dzongY,
          // backgroundImage: `url(${dzongImage})`,
          transition: { duration: 0.5, ease: "linear" },
          position: imagesDisplaced ? "fixed" : "fixed",
        }}
      >
        <motion.img src={dzongImage} style={{width:"100%"}}/>
        </motion.div>
      
    </div>
  )}
  {!imagesLoaded && <div><LoadingAnimation/></div>}
  </div>
  );
}
