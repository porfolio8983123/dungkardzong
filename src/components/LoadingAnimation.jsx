// LoadingAnimation.jsx
import '../Styles/LoadingAnimation.scss'; // Ensure to style the loading animation appropriately

const LoadingAnimation = () => {
  return (
    <div className="loading-animation">
      {/* Replace this with your actual loading animation */}
      <div><h1 className='dungkarText'>DUNGKAR</h1></div>
      <div className="spinner"></div>
    </div>
  );
};

export default LoadingAnimation;
