import { motion } from "framer-motion";
import { useRef, useState, useEffect } from "react";
import { useMediaQuery } from 'react-responsive';

import "../Styles/Climate.scss";

import WinterSmall from "./svg/WinterSmall";
import SpringSmall from "./svg/SpringSmall";
import SummerSmall from "./svg/SummerSmall";
import AutumnSmall from "./svg/AutumnSmall";

import winterBig from '../assets/winter/winterBig.svg';
import springBig from '../assets/spring/springBig.svg';
import summerBig from '../assets/summer/summerBig.svg';
import autumnBig from '../assets/autumn/autumnBig.svg';

import pic1 from '../assets/1.png';
import pic2 from '../assets/2.png';
import pic3 from '../assets/3.png';

import logo from "../assets/logo.svg";
import SunMoon from "../assets/sun&moon.svg";

export default function Climate() {
  const buttonDivRef = useRef(null);

  const isMobile = useMediaQuery({ query: '(max-width: 500px)' });
  const isMobile2 = useMediaQuery({ query: '(max-width: 405px)' });
  const isTablet = useMediaQuery({ query: '(min-width: 601px) and (max-width: 1024px)' });
  const isDesktop = useMediaQuery({ query: '(min-width: 1025px)' });

  const [monthText, setMonthText] = useState(null);
  const [tempText, setTempText] = useState(null);
  const [lowestTempText, setLowestTempText] = useState(null);
  const [lowestTemp, setLowestTemp] = useState(null);
  const [precipitation, setPrecipitation] = useState(null);
  const [humidity, setHumidity] = useState(null);
  const [wind, setWind] = useState(null);
  const [precipitationText, setPrecipitationText] = useState(null);
  const [humidityText, setHumidityText] = useState(null);
  const [windText, setWindText] = useState(null);
  const [sunmoon, setSunMoon] = useState(null);
  const [sun, setSun] = useState(null);
  const [moon, setMoon] = useState(null);
  const [sunriseText, setSunRiseText] = useState(null);
  const [sunsetText, setSunSetText] = useState(null);
  const [moonriseText, setMoonRiseText] = useState(null);
  const [moonsetText, setMoonSetText] = useState(null);
  const [seasonBig, setSeasonBig] = useState(null);

  const [cardColor, setCardColor] = useState(null);
  const [textColor, setTextColor] = useState("#FFFFFF");
  const [activeButton, setActiveButton] = useState(null);

  useEffect(() => {
    handleImageChange("Winter");
  }, []);

  const handleImageChange = (season) => {
    setActiveButton(season);
    switch (season) {
      case "Winter":
        setTextColor("#5ABAFF"); // Blue for Winter
        setMonthText("Dec-Feb");
        setTempText("-7℃");
        setLowestTemp("Lowest Temperature");
        setLowestTempText("8.5℃ Highest");
        setPrecipitation("Precipitation");
        setHumidity("Humidity");
        setWind("Wind");
        setPrecipitationText("16mm");
        setHumidityText("83%");
        setWindText("2-4km/hr");
        setSunMoon(SunMoon);
        setSun("SUN");
        setSunRiseText("Rise: 6:46 A.M");
        setSunSetText("Set: 17:31 P.M");
        setMoon("MOON");
        setMoonRiseText("Rise: 7:15 P.M");
        setMoonSetText("Set: 5:00 A.M");
        setCardColor("#5ABAFF");
        setSeasonBig(winterBig);
        break;
      case "Spring":
        setTextColor("#06B300"); // Green for Spring
        setMonthText("March-May");
        setTempText("-2℃");
        setLowestTemp("Lowest Temperature");
        setLowestTempText("15℃ Highest");
        setPrecipitation("Precipitation");
        setHumidity("Humidity");
        setWind("Wind");
        setPrecipitationText("20-24%");
        setHumidityText("80-95%");
        setWindText("2-4km/hr");
        setSunMoon(SunMoon);
        setSun("SUN");
        setCardColor("#06B300");
        setSunRiseText("Rise: 5:15 A.M");
        setSunSetText("Set: 6:00 P.M");
        setMoon("MOON");
        setMoonRiseText("Rise: 7:15 P.M");
        setMoonSetText("Set: 5:00 A.M");
        setSeasonBig(springBig);
        break;
      case "Summer":
        setTextColor("#FFB800"); // Yellow for Summer
        setMonthText("June-Aug");
        setTempText("11℃");
        setLowestTemp("Lowest Temperature");
        setLowestTempText("22℃ Highest");
        setPrecipitation("Precipitation");
        setHumidity("Humidity");
        setWind("Wind");
        setPrecipitationText("20mm");
        setHumidityText("90%");
        setWindText("2-4km/hr");
        setSunMoon(SunMoon);
        setSun("SUN");
        setCardColor("#FFB800");
        setSunSetText("Set: 6:00 P.M");
        setMoon("MOON");
        setMoonRiseText("Rise: 7:15 P.M");
        setMoonSetText("Set: 5:00 A.M");
        setSeasonBig(summerBig);
        break;
      case "Autumn":
        setTextColor("#E35E13"); // Orange for Autumn
        setMonthText("Sept-Nov");
        setTempText("3℃");
        setLowestTemp("Lowest Temperature");
        setLowestTempText("14℃ Highest");
        setPrecipitation("Precipitation");
        setHumidity("Humidity");
        setWind("Wind");
        setPrecipitationText("16mm");
        setHumidityText("83%");
        setWindText("2-4km/hr");
        setSunMoon(SunMoon);
        setSun("SUN");
        setCardColor("#E35E13");
        setSunSetText("Set: 6:00 P.M");
        setMoonRiseText("Rise: 7:15 P.M");
        setMoon("MOON");
        setMoonSetText("Set: 5:00 A.M");
        setSeasonBig(autumnBig);
        break;
      default:
        break;
    }
  };

  return (
    <div className="climateMain">
      <motion.div ref={buttonDivRef} className="buttonDiv" style={{ display: 'flex', justifyContent: 'space-evenly', width: "100% !important" }}>
        <motion.button
          onClick={() => handleImageChange("Winter")}
          animate={{ backgroundColor: activeButton === "Winter" ? "#FFFFFF" : "transparent", color: activeButton === "Winter" ? textColor : "#FFFFFF" }}
          transition={{ duration: 0.5 }}>
          <div style={{ display: "flex" }}>
            <div><WinterSmall color={activeButton === "Winter" ? textColor : "#FFFFFF"} /></div>
            <div>Winter</div>
          </div>
        </motion.button>
        <motion.button
          onClick={() => handleImageChange("Spring")}
          animate={{ color: activeButton === "Spring" ? textColor : "#FFFFFF", backgroundColor: activeButton === "Spring" ? "#FFFFFF" : "transparent" }}
          transition={{ duration: 0.5 }}>
          <div style={{ display: "flex" }}>
            <div><SpringSmall color={activeButton === "Spring" ? textColor : "#FFFFFF"} /></div>
            <div>Spring</div>
          </div>
        </motion.button>
        <motion.button
          onClick={() => handleImageChange("Summer")}
          animate={{ color: activeButton === "Summer" ? textColor : "#FFFFFF", backgroundColor: activeButton === "Summer" ? "#FFFFFF" : "transparent" }}
          transition={{ duration: 0.5 }}>
          <div style={{ display: "flex" }}>
            <div><SummerSmall color={activeButton === "Summer" ? textColor : "#FFFFFF"} /></div>
            <div>Summer</div>
          </div>
        </motion.button>
        <motion.button
          onClick={() => handleImageChange("Autumn")}
          animate={{ color: activeButton === "Autumn" ? textColor : "#FFFFFF", backgroundColor: activeButton === "Autumn" ? "#FFFFFF" : "transparent" }}
          transition={{ duration: 0.5 }}>
          <div style={{ display: "flex" }}>
            <div><AutumnSmall color={activeButton === "Autumn" ? textColor : "#FFFFFF"} /></div>
            <div>Autumn</div>
          </div>
        </motion.button>
      </motion.div>
      <div className="cardDiv">
        <div className="card" style={{ backgroundColor: cardColor, display: "flex", alignItems: "center" }}>
          <div className="content" style={{ alignItems: "center" }}>
            <div><img src={seasonBig}></img></div>
            <div><h1>{monthText}</h1></div>
          </div>
        </div>
        <div className="card" style={{ backgroundColor: cardColor, display: "flex", alignItems: "center" }}>
          <div className="content" style={{ margin: "5%", alignItems: "flex-start" }}>
            <h1 style={{ fontSize: isMobile2 ? '2.1rem' : isMobile ? '2.5rem' : '4rem', fontWeight: "800" }}>{tempText}</h1>
            <p>{lowestTemp}</p>
            <p>{lowestTempText}</p>
          </div>
          <div className="content" style={{ display: "flex", flexDirection: "column", padding: isMobile ? '0.6em' : '0.8em' }}>
            <p className="bold">{precipitationText}</p>
            <p>{precipitation}</p>
            <p className="bold">{humidityText}</p>
            <p>{humidity}</p>
            <p className="bold">{windText}</p>
            <p>{wind}</p>
          </div>
        </div>
        <div className="card" style={{ backgroundColor: cardColor, display: "flex" }}>
          <div className="content" style={{ margin: "5%" }}>
            <div><img src={sunmoon} style={{ width: isMobile ? '100%' : '120%' }}></img></div>
          </div>
          <div className="content" style={{ height: "100%" }}>
            <div style={{ padding: "0.8em", height: "100%" }}>
              <div style={{ margin: "5px", height: "50%" }}>
                <p className="bold">{sun}</p>
                <p style={{ whiteSpace: "nowrap" }}>{sunriseText}</p>
                <p>{sunsetText}</p>
              </div>
              <div style={{ margin: "5px", height: "50%" }}>
                <p className="bold">{moon}</p>
                <p>{moonriseText}</p>
                <p>{moonsetText}</p>
              </div>
            </div>
          </div>
        </div>
      </div>

      {/* <div style={{ display: 'flex', flexDirection: 'column', color: 'white' }}>
        <h1 style={{ width: '80%'}}>Notable Plant Species</h1>
        <div className="cardDiv">
          <div className="card" style={{ backgroundColor: cardColor, display: "flex", alignItems: "center", opacity: 1 }}>
            <div className="content" style={{ alignItems: "center" }}>
              <div>
                <img src={pic1}></img>
                <h3>Trees:</h3>
                <h4>Oak, Pine, Fir, and Rhododendron</h4>
              </div>
            </div>
          </div>
          <div className="card" style={{ backgroundColor: cardColor, display: "flex", alignItems: "center", opacity: 1 }}>
            <div className="content" style={{ alignItems: "center" }}>
              <div>
                <img src={pic2}></img>
                <h3>Shrubs:</h3>
                <h4>Daphne, and Berberis</h4>
              </div>
            </div>
          </div>
          <div className="card" style={{ backgroundColor: cardColor, display: "flex", alignItems: "center", opacity: 1 }}>
            <div className="content" style={{ alignItems: "center" }}>
              <div>
                <img src={pic3}></img>
                <h3>Herbs and Flowers:</h3>
                <h4>Blue Poppy, Primulas, and Orchids</h4>
              </div>
            </div>
          </div>
        </div>
      </div> */}
    </div>
  );
}
