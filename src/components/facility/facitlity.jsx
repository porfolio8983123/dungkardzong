import React from 'react';
import { Link } from 'react-router-dom';
import './facility.scss';
import Header from '../Header';
import Footer from '../Footer';

import seminar1cover from '../Img/SeminarOne/p17.jpg';
import seminar1one from '../Img/SeminarOne/p15.jpg';
import seminar1two from '../Img/SeminarOne/p16.jpg';
import seminar1three from '../Img/SeminarOne/p18.jpg';
import seminar1four from '../Img/SeminarOne/p22.jpg';

import seminar2cover from '../Img/SeminarTwo/p34.jpg';
import seminar2one from '../Img/SeminarTwo/p28.jpg';
import seminar2two from '../Img/SeminarTwo/p29.jpg';
import seminar2three from '../Img/SeminarTwo/p30.jpg';
import seminar2four from '../Img/SeminarTwo/p31.jpg';

import seminar3cover from '../Img/SeminarThree/p24.jpg';
import seminar3one from '../Img/SeminarThree/p25.png';
import seminar3two from '../Img/SeminarThree/p26.jpg';
import seminar3three from '../Img/SeminarThree/p27.jpg';
import seminar3four from '../Img/SeminarThree/p24.jpg';

import Meditationcover from '../Img/Maditation/p66.jpg';
import Meditationone from '../Img/Maditation/p62.jpg';
import Meditationtwo from '../Img/Maditation/p63.jpg';
import Meditationthree from '../Img/Maditation/p64.jpg';
import Meditationfour from '../Img/Maditation/p65.jpg';

import Sportcover from '../Img/GameHall/p49.jpg';
import Sportone from '../Img/GameHall/p42.jpg';
import Sporttwo from '../Img/GameHall/p44.jpg';
import Sportthree from '../Img/GameHall/p46.jpg';
import Sportfour from '../Img/GameHall/p47.jpg';

import Conferencecover from '../Img/Conference/p39.jpg';
import Conferenceone from '../Img/Conference/p36.jpg';
import Conferencetwo from '../Img/Conference/p37.jpg';
import Conferencethree from '../Img/Conference/p38.jpg';
import Conferencefour from '../Img/Conference/p40.jpg';

import Boardcover from '../Img/Board/p12.jpg';
import Boardone from '../Img/Board/p7.jpg';
import Boardtwo from '../Img/Board/p8.jpg';
import Boardthree from '../Img/Board/p9.jpg';
import Boardfour from '../Img/Board/p11.jpg';

import Libcover from '../Img/Lib/p56.jpg';
import Libone from '../Img/Lib/p51.jpg';
import Libtwo from '../Img/Lib/p53.jpg';
import Libthree from '../Img/Lib/p55.jpg';
import Libfour from '../Img/Lib/p57.jpg';

import Foundcov from '../Img/Founders/cov.jpg';
import Foundone from '../Img/Founders/1.jpg';
import Foundtwo from '../Img/Founders/2.jpg';
import Foundthree from '../Img/Founders/3.jpg';
import Foundfour from '../Img/Founders/4.jpg';

import loungecov from '../Img/Longe/cov.webp';
import loungeone from '../Img/Longe/1.webp';
import loungetwo from '../Img/Longe/2.webp';
import loungethree from '../Img/Longe/3.webp';
import loungefour from '../Img/Longe/4.webp';

import bilcov from '../Img/Bilateral/cov.webp';
import bilone from '../Img/Bilateral/1.webp';
import biltwo from '../Img/Bilateral/2.webp';
import bilthree from '../Img/Bilateral/3.webp';
import bilfour from '../Img/Bilateral/4.webp';

import dincov from '../Img/Dining/cov.webp';
import dinone from '../Img/Dining/1.webp';
import dintwo from '../Img/Dining/2.webp';
import dinthree from '../Img/Dining/3.webp';
import dinfour from '../Img/Dining/4.webp';

import footcov from '../Img/Football/cov.webp';
import footone from '../Img/Football/1.webp';
import foottwo from '../Img/Football/2.webp';
import footthree from '../Img/Football/3.webp';
import footfour from '../Img/Football/4.webp';

export const facilities = [
    {
        id: 1,
        name: "Meditation Center",
        description: "Support Geysarling's Library through donations. Your contributions help us expand our collections and improve our services. Acknowledge the generosity of our donors and learn how you can contribute.",
        coverPhoto: Meditationcover,
        images: [Meditationone, Meditationtwo, Meditationthree, Meditationfour],
        slider_overlay: [
            (
                <>
                    <h3>Rigsoom Goenpa</h3>
                    <p>Rigsoom Goenpa is a temple that houses statues of the three principal bodhisattvas, representing wisdom, compassion, and power:</p>
                    <p>● Toenpa</p>
                    <p>● Chenreyzi (Avalokiteshvara)</p>
                    <p>● Jampelyang (Manjushri)</p>
                    <p>● Chana Dorji (Vajrapani)</p>
                </>
            ),
            (
                <>
                    <h3>Dechhog Lhakhang</h3>
                    <p>Dechhog Lhakhang is devoted to the deity Chakrasamvara, symbolizing the union of wisdom and compassion. The main statue here is:</p>
                    <p>● Dechhog Yab Yum Zhel nga Chhag Chunyi</p>
                </>
            ),
            (
                <>
                    <h3>Rigsoom Namthruel Lhakhang</h3>
                    <p>Rigsoom Namthruel Lhakhang enshrines statues of the three deities that embody protection and enlightenment:</p>
                    <p>● Chana Dorji</p>
                    <p>● Shinje Shed Yab Yum</p>
                    <p>● Tandin Yab Yum</p>
                    <p>● Chana Dorji Tumbo Yab Yum</p>
                    <p>● Chana Dorji</p>
                    <p>This temple also features a beautifully crafted altar (Chhoesham).</p>
                </>
            ),
            (
                <>
                    <h3>Dolma Lhakhang</h3>
                    <p>Dolma Lhakhang is dedicated to the female Bodhisattvas, representing different aspects of compassion and wisdom:</p>
                    <p>● Dolma Kurukulle</p>
                    <p>● Dolkar (White Tara)</p>
                    <p>● Namgyalma</p>
                </>
            )
        ],
        capacity: 60
    },
    {
        id: 2,
        name: "High-Altitude Athletics Training",
        description: "Elevate Your Events and Training at Our Premier High Altitude Venue. Experience unparalleled facilities and breathtaking views at our high-altitude Stadium, the perfect venue for sports events, grand gatherings, and elite training programs. With cutting-edge amenities and ample seating, this stadium offers an exceptional environment for athletes and spectators alike.",
        coverPhoto: footcov,
        images: [footone, foottwo, footthree, footfour],
        slider_overlay:  [
            (
                <>
                    <h3>World-Class Specifications</h3>
                    <p>● Football Field (94m x 60m): A professional-grade field designed for top-level matches and training.</p>
                    <p>● Synthetic Track (400m): A high-quality track ideal for competitive and training purposes.</p>
                    <p>● Lighting (1000W x 10 per pole): Four poles ensure brilliant illumination for night events.</p>
                    <p>● Tensile Roofing: Provides comfortable, shaded seating in the audience gallery.</p>
                    <p>● Gallery Seating Capacity: Accommodates 3000 spectators, ensuring everyone has a great view.</p>
                    <p>● Parking Capacity: Ample parking for 50 vehicles.</p>
                    <p>● Washrooms: Modern and clean facilities.</p>
                    <p>● Wifi: Reliable high-speed internet to keep you connected.</p>
                    <p>● Sound System: Available upon request for an immersive audio experience.</p>
                </>
            ),
            (
                <>
                    <h3>The Perfect Venue for Any Event</h3>
                    <p>Our Stadium is the ideal choice for a wide range of events:</p>
                    <p>● Football Matches: Host professional-level games on our pristine field.</p>
                    <p>● Athletic Competitions: Our synthetic track is perfect for track and field events.</p>
                    <p>● High-Altitude Training: Benefit from the performance-enhancing effects of training at altitude.</p>
                    <p>● Large-Scale Gatherings: With seating for 3000, our stadium is perfect for concerts, festivals, and more.</p>
                </>
            ),
            (
                <>
                    <h3>Training Facilities</h3>
                    <p>Our high-altitude location offers unique advantages for athletes aiming to enhance their performance:</p>
                    <p>● Football Field: Perfect for rigorous training sessions and professional matches.</p>
                    <p>● Synthetic Track: Provides an excellent surface for runners and track athletes.</p>
                </>
            ),
            (
                <>
                    <h3>Exceptional Amenities</h3>
                    <p>We ensure every event is a success with our state-of-the-art facilities:</p>
                    <p>● Professional Lighting: Four poles with ten 1000W lights each guarantee excellent visibility for evening events.</p>
                    <p>● Tensile Roofing: Protects spectators from the elements, ensuring comfort.</p>
                    <p>● High-Speed Wifi: Stay connected with fast, reliable internet throughout the venue.</p>
                    <p>● Sound System: High-quality sound setup available upon request for dynamic audio experiences.</p>
                </>
            )
        ],
        capacity: 3000
    },
    {
        id: 3,
        name: "Seminar Hall 1",
        description: "Unveil Prestige and Innovation for your next corporate event. Immerse your guests in unparalleled luxury and cutting-edge technology at Seminar Hall 1, the premier venue for prestigious events and corporate conferences within the Dzong.",                
        coverPhoto: seminar1cover,
        images: [seminar1one, seminar1two, seminar1three, seminar1four],
        slider_overlay: [
            (
                <>
                    <h3>Designed to Impress</h3>
                    <p>This intimate space boasts a seating capacity of 60, fostering a collaborative and focused environment for your important gatherings. From high-level conferences to exclusive product launches, Seminar Hall 1 provides the perfect canvas to leave a lasting impression.</p>
                </>
            ),
            (
                <>
                    <h3>Unmatched Technology</h3>
                    <p>We are equipped with the most advanced technology to ensure a seamless and impactful experience. Our features include:</p>
                    <ul>
                        <li>State-of-the-art audio-visual systems: Deliver crisp presentations and captivating multimedia experiences.</li>
                        <li>High-Definition Projector: Crystal-clear presentations, images, and videos.</li>
                        <li>Retractable Projection Screen: Seamless presentation experience.</li>
                        <li>Advanced Audio System: Superior sound quality with multi-zoned speakers and subwoofer.</li>
                        <li>Wireless Microphones: Move freely and engage your audience.</li>
                        <li>Seamless connectivity: Reliable and high-speed internet access.</li>
                    </ul>
                </>
            ),
            (
                <>
                    <h3>Unwavering Comfort and Style</h3>
                    <p>Indulge your guests in unparalleled comfort and sophistication.</p>
                    <ul>
                        <li>Luxurious furnishings: Plush seating and elegant décor create an inviting and inspiring atmosphere.</li>
                        <li>Meticulous attention to detail: Our team ensures every aspect of your event is executed flawlessly.</li>
                    </ul>
                    <p>Contact us today to discuss your specific requirements and craft a truly exceptional experience.</p>
                </>
            ),
            (
                <>
                    <h3>Optional Technological Enhancements</h3>
                    <p>For an even more dynamic experience, consider these additional features:</p>
                    <ul>
                        <li>Touchscreen Display: Enhance audience engagement with interactive presentations or brainstorming.</li>
                        <li>Simultaneous Interpretation Equipment: Accommodate a multilingual audience.</li>
                        <li>Live Streaming Capabilities: Broadcast your event live to a wider audience.</li>
                    </ul>
                </>
            )
        ],
        capacity: 60
    },
    {
        id: 4,
        name: "Seminar Hall 2",
        description: "Welcome to Seminar Hall 2, a versatile space designed to foster academic and professional gatherings within the Faculty of DGI.",        
        coverPhoto: seminar2cover,
        images: [seminar2one, seminar2two, seminar2three, seminar2four],
        slider_overlay: [
            (
                <>
                    <h3>Accommodating Up to 92 Guests</h3>
                    <p>The hall boasts a seating capacity of 92, allowing for both intimate discussions and larger gatherings. The flexible layout can be configured to your specific needs, whether you require a traditional classroom setup for lectures or a roundtable format for interactive workshops.</p>
                </>
            ),
            (
                <>
                    <h3>Features for a Productive Gathering</h3>
                    <ul>
                        <li>Modern Design: The space features a clean and modern aesthetic, creating a bright and inviting atmosphere conducive to learning and collaboration.</li>
                        <li>Comfortable Seating: Ample cushioned chairs ensure the comfort of your guests throughout the event.</li>
                    </ul>
                </>
            ),
            (
                <>
                    <h3>Essential Technology</h3>
                    <ul>
                        <li>Audio-Visual System: Deliver impactful presentations with a high-definition projector and a sound system.</li>
                        <li>Microphones: Clearly amplify the voices of presenters and speakers with microphones.</li>
                    </ul>
                </>
            ),
            (
                <>
                    <h3>Additional Considerations</h3>
                    <ul>
                        <li>Climate Control: Maintain a comfortable temperature for your guests with adjustable climate control.</li>
                        <li>Podium: A centrally located podium might be available to provide a focal point for speakers and presenters.</li>
                    </ul>
                </>
            )
        ],
        capacity: 94
    },
    {
        id: 5,
        name: "Seminar Hall 3",
        description: "Unleash creativity and knowledge sharing in Seminar Hall 3, a versatile venue at the DGI Faculty. This adaptable space is ideal for presentations, workshops, and a variety of events, with a comfortable capacity of 102 attendees.",        
        coverPhoto: seminar3cover,
        images: [seminar3one, seminar3two, seminar3three, seminar3four],
        slider_overlay: [
            (
                <>
                    <h3>Designed for Flexibility</h3>
                    <ul>
                        <li>Foldable Seating Pads: The unique feature of foldable seating pads allows for a customizable layout. Create a traditional classroom environment, theater-style seating, or open floor plan to suit your specific needs.</li>
                        <li>Spacious Environment: The ample space in Seminar Hall 3 allows for comfortable movement and fosters interactive group activities.</li>
                    </ul>
                </>
            ),
            (
                <>
                    <h3>Functional Technology</h3>
                    <ul>
                        <li>Essential Audio-Visual System: Deliver impactful presentations with a projector and sound system.</li>
                        <li>Microphones: Clearly amplify the voices of presenters and speakers with microphones.</li>
                    </ul>
                </>
            ),
            (
                <>
                    <h3>Additional Considerations</h3>
                    <ul>
                        <li>Climate Control: Maintain a comfortable temperature for your guests with adjustable climate control (usually standard in most conference halls).</li>
                        <li>Whiteboard/Blackboard: A whiteboard or blackboard might be available to enhance interactive presentations (confirmation recommended).</li>
                    </ul>
                </>
            )
        ],
        capacity: 102
    },
    {
        id: 6,
        name: "Conference Hall",
        description: "Versatile and Equipped for Any Event. Elevate your events with the flexibility and advanced amenities of the Conference Hall, designed to accommodate a variety of meetings involving broader groups of participants. With a maximum seating capacity of 75 to 80, this space is perfect for larger gatherings and events. Ideal for Larger Gatherings.",        
        coverPhoto: Conferencecover,
        images: [Conferenceone, Conferencetwo, Conferencethree, Conferencefour],
        slider_overlay: [
            (
                <>
                    <h3>Specifications</h3>
                    <ul>
                        <li>Seating Capacity: 35 (expandable to 75-80 for larger events)</li>
                        <li>Table Microphones (Bosch): 35</li>
                        <li>Wireless Microphones (Bosch): 2</li>
                        <li>4K-Support Projector: 1</li>
                        <li>Projector Screen: 1</li>
                        <li>Speakers (JBL): 6</li>
                        <li>Wifi: Yes</li>
                        <li>HVAC: Yes</li>
                        <li>Video Conferencing: 1</li>
                    </ul>
                </>
            ),
            (
                <>
                    <h3>Our Conference Hall is tailored to host a variety of events, such as:</h3>
                    <ul>
                        <li>Large corporate meetings</li>
                        <li>Training sessions</li>
                        <li>Workshops</li>
                        <li>Seminars</li>
                        <li>Networking events</li>
                    </ul>
                </>
            ),
            (
                <>
                    <h3>Advanced Technology for Seamless Experiences</h3>
                    <p>Ensure your event runs smoothly with our state-of-the-art equipment:</p>
                    <ul>
                        <li>4K-Support Projector: Present with stunning clarity and detail.</li>
                        <li>Table and Wireless Microphones (Bosch): Perfect audio capture and distribution.</li>
                        <li>JBL Speakers: Premium sound quality for every seat.</li>
                        <li>Reliable Wifi: Stay connected with high-speed internet.</li>
                        <li>Video Conferencing: Connect with remote participants effortlessly.</li>
                    </ul>
                </>
            ),
            (
                <>
                    <h3>Comfort and Convenience</h3>
                    <p>We prioritize the comfort and convenience of your guests:</p>
                    <ul>
                        <li>HVAC System: Maintain an optimal climate for all attendees.</li>
                        <li>Flexible Seating Arrangements: Adaptable seating to suit various event types and sizes.</li>
                    </ul>
                </>
            )
        ],
        capacity: 35
    },
    {
        id: 7,
        name: "Board Room",
        description: "Excellence for Executive Meetings. Experience unparalleled sophistication and state-of-the-art technology in our Board Room, designed for executive-level meetings. With a capacity to comfortably seat up to 23 and the ability to expand to accommodate 50 participants, this space is ideal for high-level discussions and decision-making gatherings.",
        coverPhoto: Boardcover,
        images: [Boardone, Boardtwo, Boardthree, Boardfour],
        slider_overlay: [
            (
                <>
                    <h3>Ideal for Executive-Level Meetings</h3>
                    <ul>
                        <li>Board meetings</li>
                        <li>Strategy sessions</li>
                        <li>Executive presentations</li>
                        <li>High-level negotiations</li>
                    </ul>
                </>
            ),
            (
                <>
                    <h3>Essential Technology</h3>
                    <p>Ensure your executive meetings are productive and engaging with our advanced technological offerings:</p>
                    <ul>
                        <li>4K-Support Projector: Deliver presentations with exceptional clarity.</li>
                        <li>Table and Wireless Microphones (Bosch): Superior audio quality and convenience.</li>
                        <li>JBL Speakers: Crystal-clear sound for all attendees.</li>
                        <li>Reliable Wifi: Stay connected with high-speed internet.</li>
                        <li>Video Conferencing: Seamlessly connect with remote participants.</li>
                    </ul>
                </>
            ),
            (
                <>
                    <h3>Comfort and Professionalism</h3>
                    <p>Prioritizing the comfort and professional needs of your executives:</p>
                    <ul>
                        <li>HVAC System: Ensure a comfortable environment for all attendees.</li>
                        <li>Luxurious Furnishings: High-end seating and décor to create a sophisticated atmosphere.</li>
                    </ul>
                </>
            ),
            (
                <>
                    <h3>Optional Enhancements</h3>
                    <p>For an enhanced meeting experience, consider these additional features:</p>
                    <ul>
                        <li>Interactive Touchscreen Displays</li>
                        <li>Simultaneous Interpretation Equipment</li>
                        <li>Live Streaming Capabilities</li>
                    </ul>
                </>
            )
        ],
        capacity: 23
    },
    {
        id: 8,
        name: "Library",
        description: "Welcome to Geysarling's Library, the heart of knowledge and research at the institution. Our mission is to support academic excellence and foster a love for lifelong learning. Explore our vast collection of resources and take advantage of our services designed to assist you in your scholarly journey.",
        coverPhoto: Libcover,
        images: [Libone, Libtwo, Libthree, Libfour],
        slider_overlay:  [
            (
                <>
                    <h3>Books and Journals</h3>
                    <p>Dive into our extensive collection of academic books and journals across various disciplines. Use our online catalog to search for specific titles and topics. Borrowing policies ensure you have access to the materials you need for your research and studies.</p>
                </>
            ),
            (
                <>
                    <h3>Digital Resources</h3>
                    <p>Access a wide array of online databases, e-books, and journals from anywhere, anytime. Our digital resources are curated to support your academic needs. Find out how to access these resources on and off campus.</p>
                </>
            ),
            (
                <>
                    <h3>Special Collections</h3>
                    <p>Discover unique collections including rare books, manuscripts, and archives. These special collections are available for research and can provide valuable insights into historical and contemporary issues. Learn how to access and utilize these treasures.</p>
                </>
            ),
            (
                <>
                    <h3>Multimedia Resources</h3>
                    <p>Enhance your learning experience with our multimedia resources, including CDs, DVDs, and streaming services. Explore our media rooms equipped with the latest technology for your convenience.</p>
                </>
            )
        ],
        capacity: 0
    },
    {
        id: 9,
        name: "Founders Court",
        description: "A Premier Venue for Special Events. The Founder's Court is a versatile and elegant covered courtyard, perfect for hosting a wide range of special events. Whether it's a corporate gala, a grand celebration, or an exclusive dining experience, this space provides the ideal setting to create unforgettable memories.",
        coverPhoto: Foundcov,
        images: [Foundone, Foundtwo, Foundthree, Foundfour],
        slider_overlay: [
            (
                <>
                    <h3>Versatile Event Space</h3>
                    <p>Our Founder's Court is designed to accommodate a variety of event types with its flexible seating and capacity options:</p>
                    <p>● Dining Capacity: 140-150 guests</p>
                    <p>● Multiple Seating Capacity: Up to 350 guests for larger events</p>
                    <p>● Lounge Seating: 25 to 30 people in each room</p>
                </>
            ),
            (
                <>
                    <h3>Ideal for Special Events</h3>
                    <p>Founder's Court is perfectly suited for:</p>
                    <p>● Corporate galas and dinners</p>
                    <p>● Product launches</p>
                    <p>● Celebratory events</p>
                    <p>● Networking receptions</p>
                    <p>● Cultural performances</p>
                </>
            ),
            (
                <>
                    <h3>Essential Technology</h3>
                    <p>Enhance your event with our cutting-edge technological features:</p>
                    <p>● Large LED Screen (7m x 4m): Perfect for displaying presentations, videos, and live feeds.</p>
                    <p>● Samsung LCD Screens (85 inch): Additional visual support for comprehensive coverage.</p>
                    <p>● Powerful Audio System: Four loudspeakers and two subwoofers ensure high-quality sound for all attendees.</p>
                    <p>● Wireless Microphones: Six wireless mics for effortless communication and presentations.</p>
                </>
            ),
            (
                <>
                    <h3>Comfort and Elegance</h3>
                    <p>We prioritize creating a comfortable and elegant environment for your guests:</p>
                    <p>● Flexible Seating Arrangements: Customize seating layouts to suit your event's needs.</p>
                    <p>● Lounge Areas: Comfortable lounge seating for smaller, intimate gatherings within the larger event space.</p>
                </>
            )
        ],
        capacity: 192
    },
    {
        id: 10,
        name: "Multi-Purpose Hall",
        description: "Elevate Your Sporting and Wellness Events at the Premier Indoor Facility. Discover the ultimate venue for indoor sports, fitness, and wellness events at the Multi-Purpose Hall (MPH). Designed to international standards, this versatile space caters to a wide range of activities, ensuring an exceptional experience for athletes, fitness enthusiasts, and spectators alike.",
        coverPhoto: Sportcover,
        images: [Sportone, Sporttwo, Sportthree, Sportfour],
        slider_overlay: [
            (
                <>
                    <h3>Perfect for a Wide Range of Activities</h3>
                    <p>Our Multi-Purpose Hall is the ideal choice for diverse events and activities:</p>
                    <p>● Indoor Sports Competitions: Host basketball, volleyball, and badminton matches with ease.</p>
                    <p>● Fitness Classes and Training: Utilize our well-equipped gym and spacious yoga room.</p>
                    <p>● Large Gatherings and Events: With seating for 1300, the hall is perfect for concerts, conferences, and exhibitions.</p>
                </>
            ),
            (
                <>
                    <h3>Exceptional Sports and Fitness Facilities</h3>
                    <p>Our MPH offers world-class amenities to ensure the best experience for all participants:</p>
                    <p>● International Standard Basketball Court: Perfect for professional and amateur games.</p>
                    <p>● Versatile Volleyball and Badminton Courts: Ideal for tournaments and recreational play.</p>
                    <p>● Fully-Equipped Gym: Designed to meet all your fitness training needs.</p>
                    <p>● Serene Yoga Room: Accommodates up to 20 participants for yoga and meditation sessions.</p>
                </>
            ),
            (
                <>
                    <h3>Advanced Amenities</h3>
                    <p>Enhance your event with our top-tier facilities:</p>
                    <p>● Ceiling-Mounted Loudspeakers: Eight speakers ensure clear and immersive sound throughout the hall.</p>
                    <p>● Customizable Lighting: Four different lighting options to suit any event or activity.</p>
                    <p>● High-Speed Wifi: Stay connected with reliable internet access.</p>
                </>
            ),
            (
                <>
                    <h3>Comfort and Convenience</h3>
                    <p>We prioritize the comfort and convenience of all attendees:</p>
                    <p>● Spacious Seating: Accommodates up to 1300 spectators comfortably.</p>
                    <p>● Modern Washrooms: Clean and easily accessible facilities for everyone.</p>
                </>
            )
        ],
        capacity: 1300
    },
    {
        id: 11,
        name: "Lounge Room",
        description: "A Sophisticated Setting for Intimate Gatherings. Discover the perfect venue for smaller, more intimate events with our luxurious Lounge Room. Designed for comfort and versatility, this elegant space provides a sophisticated setting for meetings, receptions, and social gatherings.",
        coverPhoto: loungecov,
        images: [loungeone, loungetwo, loungethree, loungefour],
        slider_overlay: [
            (
                <>
                    <h3>Specifications</h3>
                    <p>● Seating Capacity: Up to 60 (depending on the seating arrangement)</p>
                    <p>● Wifi: Yes</p>
                    <p>● HVAC: Yes</p>
                </>
            ),
            (
                <>
                    <h3>Ideal for Various Functions</h3>
                    <p>Our Lounge Room is perfect for a range of informal and semi-formal events:</p>
                    <p>● Casual Meetings: Ideal for small business meetings or brainstorming sessions.</p>
                    <p>● Social Gatherings: Host intimate gatherings, cocktail parties, or networking events.</p>
                    <p>● Receptions: Suitable for smaller receptions or celebratory events.</p>
                    <p>● Relaxation: A comfortable space for unwinding or enjoying a break.</p>
                </>
            ),
            (
                <>
                    <h3>Advanced Amenities</h3>
                    <p>Ensure a seamless and enjoyable experience with our advanced amenities:</p>
                    <p>● Flexible Seating Arrangements: Customize the layout to suit your event’s needs.</p>
                    <p>● High-Speed Wifi: Reliable internet access for connectivity and convenience.</p>
                    <p>● HVAC System: Maintain a comfortable temperature for a pleasant environment.</p>
                </>
            ),
            (
                <>
                    <h3>A Stylish and Comfortable Environment</h3>
                    <p>Our Lounge Room combines elegance with comfort to create an inviting space:</p>
                    <p>● Elegant Décor: Stylish furnishings and decor enhance the ambiance for a sophisticated experience.</p>
                    <p>● Attention to Detail: Our team ensures every aspect of the room is meticulously maintained and prepared for your event.</p>
                </>
            )
        ],
        capacity: 60
    },
    {
        id: 12,
        name: "Bilateral Room",
        description: "The Ideal Venue for High-Level Discussions. Elevate your business meetings and negotiations with our Bilateral Meeting Room, designed to provide a focused and professional environment. This space is perfect for intimate gatherings and high-level discussions, offering the privacy and comfort needed for productive meetings.",
        coverPhoto: bilcov,
        images: [bilone, biltwo, bilthree, bilfour],
        slider_overlay: [
            (
                <>
                    <h3>Specifications</h3>
                    <p>● Seating Capacity: Up to 20 (depending on the seating arrangement)</p>
                    <p>● Wifi: Yes</p>
                    <p>● HVAC: Yes</p>
                </>
            ),
            (
                <>
                    <h3>Perfect for High-Level Meetings</h3>
                    <p>Our Bilateral Meeting Room is ideal for a range of business events:</p>
                    <p>● Negotiations: Host important discussions in a confidential and comfortable setting.</p>
                    <p>● Executive Meetings: Perfect for board meetings and strategic planning sessions.</p>
                    <p>● Private Consultations: Ideal for one-on-one meetings and small group discussions.</p>
                </>
            ),
            (
                <>
                    <h3>Advanced Amenities</h3>
                    <p>Enhance the productivity and comfort of your meetings with our superior amenities:</p>
                    <p>● Flexible Seating Arrangements: Tailor the layout to meet the specific needs of your meeting.</p>
                    <p>● High-Speed Wifi: Reliable internet access for seamless connectivity.</p>
                    <p>● HVAC System: Ensure a comfortable environment regardless of the weather.</p>
                </>
            ),
            (
                <>
                    <h3>Professional and Comfortable Environment</h3>
                    <p>Our Bilateral Meeting Room combines functionality with elegance to create an optimal meeting space:</p>
                    <p>● Sophisticated Décor: Professional and stylish furnishings to enhance the meeting environment.</p>
                    <p>● Attention to Detail: Our team ensures that every aspect of your meeting is meticulously planned and executed.</p>
                </>
            )
        ],
        capacity: 20
    },
    {
        id: 13,
        name: "Dining Hall",
        description: "A Grand Venue for Unforgettable Dining Experiences. The Dining Hall is a magnificent space designed to host large-scale dining events, offering an elegant and spacious environment for your guests. With a seating capacity of 800 and a maximum capacity of 1000, it is perfect for banquets, receptions, and large gatherings.",
        coverPhoto: dincov,
        images: [dinone, dintwo, dinthree, dinfour],
        slider_overlay: [
            (
                <>
                    <h3>Specifications</h3>
                    <p>● Dining Capacity: 800 guests</p>
                    <p>● Maximum Capacity: 1000 guests</p>
                    <p>● Wifi: Yes</p>
                    <p>● HVAC: Yes</p>
                </>
            ),
            (
                <>
                    <h3>Ideal for Large-Scale Dining Events</h3>
                    <p>The Dining Hall is perfect for a variety of grand dining occasions:</p>
                    <p>● Banquets: Host large-scale banquets with ease and elegance.</p>
                    <p>● Receptions: Perfect for wedding receptions and gala dinners.</p>
                    <p>● Corporate Events: Ideal for company parties, award ceremonies, and conferences.</p>
                    <p>● Community Gatherings: Suitable for large community events and celebrations.</p>
                </>
            ),
            (
                <>
                    <h3>Advanced Amenities</h3>
                    <p>Ensure a seamless and enjoyable experience for your guests with our superior amenities:</p>
                    <p>● Flexible Seating Arrangements: Tailor the layout to meet the specific needs of your event.</p>
                    <p>● High-Speed Wifi: Reliable internet access for seamless connectivity.</p>
                    <p>● HVAC System: Ensure a comfortable environment regardless of the weather.</p>
                </>
            ),
            (
                <>
                    <h3>An Elegant and Spacious Environment</h3>
                    <p>Our Dining Hall combines grandeur with comfort to create a memorable dining experience:</p>
                    <p>● Elegant Décor: Stylish and sophisticated furnishings and décor to enhance the ambiance.</p>
                    <p>● Attention to Detail: Our team ensures that every aspect of your event is meticulously planned and executed.</p>
                </>
            )
        ],
        capacity: 800
    }
];

const Facility = () => {
    return (
        <>
        <Header/>
            <div className="facility-container">
                <h1>Our Facilities</h1>
                <div className="facility-grid">
                    {facilities.map(facility => (
                        <Link key={facility.id} to={`/details/${facility.id}`} className="facility-card">
                            <img src={facility.coverPhoto} alt={facility.name} className="facility-photo" />
                            <div className="facility-gradient"></div>
                            <div className="facility-info">
                                <h2>{facility.name}</h2>
                            </div>
                        </Link>
                    ))}
                </div>
            </div>
            <Footer />
        </>
    );
};

export default Facility;
