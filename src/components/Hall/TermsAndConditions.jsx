import React from 'react';
import './TermsAndConditions.scss';
import BookConferenceButton from '../svg/BookConferenceButton';
import Download from '../svg/Download';

const TermsAndConditions = ({ title, content, onDownloadClick }) => {
  return (
    <div className="terms-and-conditions">
      <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', width: '100%' }}>
        <a href="#booking">
          <button
            onClick={() => setFormIndex(1)}
            style={{ background: 'none', border: 'none', paddingRight: '10px', outline: 'none' }}
          >
            <BookConferenceButton />
          </button>
        </a>
      </div>
      <div style={{ textAlign: 'center', paddingRight:'10px', paddingTop:'10px', paddingBottom:'10px' }}>
        <button onClick={onDownloadClick} style={{ background: 'none', border: 'none', cursor: 'pointer', outline: 'none' }}>
          <Download />
        </button>
      </div>
    </div>
  );
};

export default TermsAndConditions;
