import React, { useState } from 'react';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import './Hall.scss';

const Hall = ({ title, capacity, description, images, overlays }) => {
  const [visibleOverlays, setVisibleOverlays] = useState(Array(images.length).fill(false));

  const toggleOverlay = (index) => {
    const updatedVisibleOverlays = [...visibleOverlays];
    updatedVisibleOverlays[index] = !updatedVisibleOverlays[index];
    setVisibleOverlays(updatedVisibleOverlays);
  };

  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 3000,
    arrows: true,
    customPaging: (i) => (
      <div className="custom-dot">
        {/* <span>{i + 1}</span> */}
      </div>
    ),
  };

  return (
    <div className="hall">
      <h1 className="title">{title}</h1>
      <p className="capacity">Seating Capacity: {capacity}</p>
      <p className="description">{description}</p>
      <div className="slider">
        <Slider {...settings}>
          {images.map((image, index) => (
            <div key={index} className="slide">
              <img src={image} alt={`Hall Slide ${index + 1}`} className="slider-image" />
              {visibleOverlays[index] && <div className="slider-overlay">{overlays[index]}</div>}
              <button onClick={() => toggleOverlay(index)} className="read-more-button">
                {visibleOverlays[index] ? 'Read less' : 'Read more...'}
              </button>
            </div>
          ))}
        </Slider>
      </div>
    </div>
  );
};

export default Hall;
