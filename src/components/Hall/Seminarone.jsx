import React, { useEffect, useState } from 'react';
import Hall from './Hall';
import { saveAs } from 'file-saver';
import TermsAndConditions from './TermsAndConditions';
import SldImg1 from '../Img/1.png';
import SldImg2 from '../Img/2.png';
import SldImg3 from '../Img/3.png';
import SldImg4 from '../Img/4.png';
import Header from '../Header';
import ImageSlider from '../Slider/ImageSlider';
import SquareAnimation from '../Booking/SquareAnimation';
import Footer from '../Footer';
import { useParams } from 'react-router-dom';
import { facilities } from '../facility/facitlity';

const Hallone = () => {
  const { id } = useParams();
  const [currentFacility, setCurrentFacility] = useState([]);

  useEffect(() => {
    window.scrollTo(0, 0);
  }, [id]);

  useEffect(() => {
    if (facilities.length > 0) {
      const filteredFacility = facilities.filter((item) => item.id === parseInt(id));
      setCurrentFacility(filteredFacility);
    }
  }, [facilities, id]);

  useEffect(() => {
    if (currentFacility.length > 0) {
      console.log("current facility ", currentFacility[0]?.name);
    }
  }, [currentFacility]);

  const handleDownload = () => {
    saveAs('../Img/Terms.pdf', 'terms-and-conditions.pdf');
  };

  return (
    <div id='hallDetails'>
      <Header />
      {currentFacility.length > 0 && (
        <Hall 
          title={currentFacility[0]?.name}
          capacity={currentFacility[0]?.capacity}
          description={currentFacility[0]?.description}
          images={currentFacility[0]?.images}
          overlays={currentFacility[0]?.slider_overlay}
        />
      )}
      <TermsAndConditions 
        title="Terms & Agreement"
        onDownloadClick={handleDownload}
      />
      <ImageSlider />
      <SquareAnimation />
      <Footer />
    </div>
  );
};

export default Hallone;
