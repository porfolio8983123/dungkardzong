import React, { useEffect, useState } from 'react';
import logo from '../assets/logo/logo.png';
import Slide from './HeroBanner/Slide';
import Slider from './HeroBanner/Slider';
import NextAndProgress from './HeroBanner/NextAndProgress';
import gesarling from '/geaserling.png';

const Hero = () => {

  return (
    <>
    <div className="relative h-screen w-screen overflow-hidden  bg-stone-800">
      <div className="absolute z-50 top-0 w-full h-1">
        {/* <motion.div style={{ originX: 0 }} variants={timerVariants} initial="initial" animate="animate" exit="exit" className="h-full bg-red-500" /> */}
      </div>
      <img className="absolute z-50 right-0 w-52 mt-5 brightness-90" src={gesarling} />
      <Slide />name
      <Slider />
      <NextAndProgress />
    </div >
  </>
  )
}

export default Hero