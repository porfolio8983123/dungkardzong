import React, { useEffect, useState } from 'react'
import First from './svg/First'
import Second from './svg/Second'
import Third from './svg/Third'
import { motion } from 'framer-motion';
import Arrow from './svg/Arrow';
import '../app.scss';
import dzong from '../assets/dzongEdited.jpg';

const AcademicFramework = () => {

    const [windowWidth, setWindowWidth] = useState(window.innerWidth);

    useEffect(() => {
        console.log("width ", windowWidth);
    },[windowWidth])

    useEffect(() => {
        const handleResize = () => {
            setWindowWidth(window.innerWidth);
        }

        window.addEventListener('resize',handleResize);

        return () => {
            window.removeEventListener("resize",handleResize);
        }

    },[])

  return (
    <section id = "academic" style={{
        backgroundImage: `url(${dzong})`,
        backgroundRepeat:"no-repeat",
        backgroundSize:"cover",
        position:"relative"
    }}>
        <div style={{
            position:"absolute",
            top:"0",
            backgroundColor:"#291b16",
            width:"100%",
            height:"100%",
            opacity:"0.9",
            zIndex:"-1"
        }}>
            
        </div>
        <p className='academic__title'>The Royal Academy Learning Framework 2023</p>
        <div className='framework__container'>
            <div className='table__container'>
                <div className='row'>
                    <motion.table
                        initial = {{x: -40, opacity: 0}}
                        whileInView={{ x: 0, opacity: 1 }}
                        transition={{delay:0.8,duration:1}}
                    >
                        <thead>
                            <tr>
                                <th colSpan="2">SKILLS</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Comprehension</td>
                                <td>Designs</td>
                            </tr>
                            <tr>
                                <td>Critical Thinking</td>
                                <td>Leadership</td>
                            </tr>
                            <tr>
                                <td>Communicaton</td>
                                <td>Research</td>
                            </tr>
                        </tbody>
                    </motion.table>
                </div>
                <div className='row'>
                <motion.table
                    initial = {{opacity: 0}}
                    whileInView={{opacity: 1 }}
                    transition={{delay:0.4,duration:1}}
                >
                        <thead>
                            <tr>
                                <th colspan="2">PROCESSES</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Assessment</td>
                                <td>Mentorship</td>
                            </tr>
                            <tr>
                                <td>Experimentation</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>Internalising</td>
                                <td></td>
                            </tr>
                        </tbody>
                    </motion.table>
                </div>
                <div className='row'>
                <motion.table
                    initial = {{x: 40, opacity: 0}}
                    whileInView={{ x: 0, opacity: 1 }}
                    transition={{delay:0.8,duration:1}}
                >
                        <thead>
                            <tr>
                                <th colspan="2">WATERMARKS</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Decipline</td>
                                <td>Growth Management</td>
                            </tr>
                            <tr>
                                <td>Integrity</td>
                                <td>Resillience</td>
                            </tr>
                            <tr>
                                <td>Ethics</td>
                                <td>Astute</td>
                            </tr>
                        </tbody>
                    </motion.table>
                </div>
            </div>
        </div>

        <motion.div className="skills"
            initial = {{y: 10, opacity: 0}}
            whileInView={{ y: 0, opacity: 1 }}
            transition={{delay:0.8,duration:1}}
        >
            <div className="skill__container">
                <div className="skill__row">Songs</div>
                <div className="skill__row">Dances</div>
                <div className="skill__row">Songs</div>
                <div className="skill__row">Dances</div>
                <div className="skill__row">Songs</div>
                <div className="skill__row">Dances</div>
                <div className="skill__row">Songs</div>
            </div>
        </motion.div>

        <div className='svg__container main__container'>
            <div className='sideTable__container'>
                <table className='sideTable'>
                    <tr className='row'><td>CONCEPT</td></tr>
                    <tr className='rowExtra'><td>
                            <Arrow />
                        </td></tr>
                    <tr className='row'><td>AI
                        </td></tr>
                    <tr className='row'><td>Nature</td></tr>
                    <tr className='row'><td>Digital Literacy</td></tr>
                    <tr className='row'><td>History</td></tr>
                </table>
            </div>
            <div className='svg__innercontainer'>
                <p className='svg__title'>Cross Pollination</p>
                {windowWidth >= 601 && windowWidth <= 900 &&
                    <div className='svg'>  
                        <First  width = {windowWidth >= 601 && windowWidth <= 900 ? 163 : 263} height = {windowWidth >= 601 && windowWidth <= 900 ? 163 : 263} />
                        <Second width = {windowWidth >= 601 && windowWidth <= 900 ? 200 : 300} height = {windowWidth >= 601 && windowWidth <= 900 ? 195 : 295} />
                        <Third width = {windowWidth >= 601 && windowWidth <= 900 ? 163 : 263} height = {windowWidth >= 601 && windowWidth <= 900 ? 167 : 263} />
                </div>
                }
                {windowWidth >= 901 && windowWidth <= 1024 &&
                    <div className='svg'>  
                        <First  width = {163 + 40} height = {163 + 40} />
                        <Second width = {200 + 40} height = {195 + 40} />
                        <Third width = {163 + 40} height = {167 + 40} />
                </div>
                }
                {windowWidth > 1024 &&
                    <div className='svg'>  
                        <First  width = {263} height = {263} />
                        <Second width = {300} height = {295} />
                        <Third width = {263} height = {263} />
                </div>
                }
                {windowWidth < 600 &&
                    <div className='svg'>  
                        <First  width = {260} height = {260} />
                        <Second width = {297} height = {292} />
                        <Third width = {260} height = {260} />
                </div>
                }
            </div>
            <div className='sideTable__container'>
                <table className='sideTable'>
                    <tr className='row'><td>CONCEPT</td></tr>
                    <tr className='rowExtra'><td>
                            <Arrow />
                        </td></tr>
                    <tr className='row'><td>Evolution</td></tr>
                    <tr className='row'><td>Data</td></tr>
                    <tr className='row'><td>Time</td></tr>
                    <tr className='row'><td>Astronomy</td></tr>
                </table>
            </div>
        </div>

        <motion.div className='svg__container'
            initial = {{y: 40, opacity: 0}}
            whileInView={{ y: 0, opacity: 1 }}
            transition={{delay:0.4,duration:1}}
        >
            <div className='attributes__container'>
                <div className='attributes_titlecontainer'>
                    <p className='attributes__title'>Attributes of School Excellence</p>
                </div>
            </div>
        </motion.div>

        <motion.div className='svg__container'
            initial = {{y: 40, opacity: 0}}
            whileInView={{ y: 0, opacity: 1 }}
            transition={{delay:0.4,duration:1}}
        >
            <div className='attributes'>
                <div className="skill__row">Joy</div>
                <div className="skill__row">Leadership</div>
                <div className="skill__row">Stduent as a Teacher</div>
                <div className="skill__row">Teacher as a Learners</div>
                <div className="skill__row">Mastery</div>
                <div className="skill__row">Sense of Purpose</div>
                <div className="skill__row">Engaged with Community</div>
                <div className="skill__row">Individualism</div>
                <div className="skill__row">Risk Taking</div>
            </div>
        </motion.div>

    </section>
  )
}

export default AcademicFramework