import React from 'react';
import Readmore from './svg/Readmore';
import '../Styles/navigation.scss';
import SeeGallary from './svg/SeeGallary';
import image1 from '../assets/facilityNavigationImage/1.png';
import image2 from '../assets/facilityNavigationImage/2.png';
import image3 from '../assets/facilityNavigationImage/3.png';
import { useNavigate } from 'react-router-dom';

const Navigation = () => {

  const navigate = useNavigate();

  return (
    <div className='navigation'>
        <div className='col'>
          <div className='title'>
            <h6 className='our__story'>The Royal Vision</h6>
            <button className='svg__button'
              onClick={() => {
                navigate("/ourStory")
              }}
            >
              <Readmore color = "white"/>
            </button>
          </div>
          <p className='description'>Good citizenship is key to creating our children's tomorrow and quality education is a fundamental pillar for good citizenship. This is even more crucial in a world that is rapidly changing and where human movement.</p>
        </div>
        <div className='col'>
          <div className='title'>
            <h6 className='our__story'>Our Facilities</h6>
            <button className='svg__button'
              onClick={() => {
                navigate("/facilities");
              }}
            >
             <SeeGallary />
            </button>
          </div>
          <div className='gallary__container'>
            <img src={image1} alt="" />
            <img src={image2} alt="" />
            <img src={image3} alt="" />
          </div>
        </div>
        <div className='col'>
          <div className='title'>
            <h6 className='our__story'>News & Events</h6>
            <button className='svg__button'
              onClick={() => {
                navigate("/news");
              }}
            >
              <Readmore color= "white" />
            </button>
          </div>
          <p className='description'>Stay updated with the lastest happenings and upcoming events around the globe. Our News & Events section provides timely and accurate reports, offering insightfull analysis, engaging multimedia content, and real-time updates.</p>
        </div>
    </div>
  )
}

export default Navigation