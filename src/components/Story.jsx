import React from 'react';
import Header from './Header';
import './Story.scss';
import Footer from './Footer';
import ImageSlider from './Slider/ImageSlider';
import Banner from './slidertwo/slider';
import { Projects } from './Sliderthree/Details';
import About from './AboutUs/About';


const Story = () => {
  return (
    <div className="story">
      <Header />
      {/* <Banner/>
      <Projects/>
      <ImageSlider/>       */}
      <About />
      {/* <Footer /> */}
    </div>
  );
}

export default Story;
