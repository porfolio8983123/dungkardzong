import dzong from './img/dzong.jpg';
import hm from './img/HM3.jpg';
import winterDzong from './img/winterdzong.jpg';
import img2 from './img/2.JPG';
import img3 from './img/3.jpg';
import img4 from './img/4.jpg';
import img5 from './img/5.JPG';

const slides = [
  {
    img: hm,
    name: "The Genesis of The Royal Vision",
    quote:
      "In a profound reflection of The Royal Vision, the majestic Gesarling embodies the rich cultural heritage and forward-thinking educational vision of Bhutan.",
  },
  {
    img: img2,
    name: "A Symbol of Cultural and Environmental Commitment",
    quote:
      "The promise of future advancements in education and architectural magnificence converge, embodying both historical reverence and contemporary ambition.",
  },
  {
    img: img3,
    name: "The Modern Educational Paradigm",
    quote:
      "Today, Gesarling stands as a beacon of modern education and systems thinking, embodied in the esteemed Druk Gyalpo’s Institute.",
  },
  {
    img: img4,
    name: "The state-of-the-art facilities with cutting-edge designs",
    quote: "Equipped with modern facilities and stands as a beacon of contemporary elegance and technological sophistication.",
  },
  {
    img: img5,
    name: "Guarded by the Myriad Mountains and rich Vegetation",
    quote:
      "Nestled in a nation renowned for its carbon negativity and its adherence to the principles of Gross National Happiness, Gesarling stands as a testament to rich cultural heritage.",
  }
];

export default slides;
