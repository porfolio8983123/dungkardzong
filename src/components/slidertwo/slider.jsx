import { useState, useEffect } from "react";
import { Container, Row, Col } from "react-bootstrap";
import headerImg from "../Img/header.png";
import './slider.scss';
import TrackVisibility from 'react-on-screen';

export const Banner = () => {
  const [loopNum, setLoopNum] = useState(0);
  const [isDeleting, setIsDeleting] = useState(false);
  const [text, setText] = useState('');
  const [delta, setDelta] = useState(300 - Math.random() * 100);
  const [index, setIndex] = useState(1);
  const toRotate = ["Welcome to Dungkar", "Panoramic view of Panbisa", "Legends and Myths"];
  const period = 2000;

  useEffect(() => {
    let ticker = setInterval(() => {
      tick();
    }, delta);

    return () => { clearInterval(ticker) };
  }, [text]);

  const tick = () => {
    let i = loopNum % toRotate.length;
    let fullText = toRotate[i];
    let updatedText = isDeleting ? fullText.substring(0, text.length - 1) : fullText.substring(0, text.length + 1);

    setText(updatedText);

    if (isDeleting) {
      setDelta(prevDelta => prevDelta / 2);
    }

    if (!isDeleting && updatedText === fullText) {
      setIsDeleting(true);
      setIndex(prevIndex => prevIndex - 1);
      setDelta(period);
    } else if (isDeleting && updatedText === '') {
      setIsDeleting(false);
      setLoopNum(loopNum + 1);
      setIndex(1);
      setDelta(500);
    } else {
      setIndex(prevIndex => prevIndex + 1);
    }
  };

  return (
    <section className="banner" id="home">
      <Container>
        <Row className="align-items-center text-content">
          <Col xs={12}>
            <h1>Our Story</h1>
          </Col>
        </Row>
        <Row className="align-items-center text-content">
          <Col xs={12} md={6} xl={7}>
            <TrackVisibility>
              {({ isVisible }) =>
                <div className={isVisible ? "animate__animated animate__fadeIn" : ""}>
                  <h2>
                    <span className="txt-rotate" dataPeriod="1000" data-rotate='["Welcome to Dungkar", "Panoramic view of Panbisa", "Legends and Myths"]'>
                      <span className="wrap" style={{color:"white"}}>{text}</span>
                    </span>
                  </h2>
                  <p>According to the elders in the village, one of the earliest names of the place Kong-bi-sa is associated with Terton Sherab Mebar (14th Century or later?). It is said that the Tibetan treasure revealer named the place Kong-bi-sa after he settled here since it reminded him of his village back in Kham, Tibet with a similar name. Over the years, locals believe that the name Kong-bi-sa evolved into Pang-bi-sa. 1 Different sources show different dates 2 In some sources it is said that he came to Bhutan in the 14th century, in some it is mentioned as 15th century</p>
                </div>}
            </TrackVisibility>
          </Col>
        </Row>
        <Row className="align-items-center banner-image">
          <Col xs={12} md={6} xl={5}>
            <TrackVisibility>
              {({ isVisible }) =>
                <div className={isVisible ? "animate__animated animate__zoomIn" : ""}>
                  <img src={headerImg} alt="Header Img"/>
                </div>}
            </TrackVisibility>
          </Col>
        </Row>
      </Container>
    </section>
  );
};

export default Banner;
