import Navigation from '../Navigation';
import SquareAnimation from '../Booking/SquareAnimation';
import Institute from '../Institute';
import AcademicFramework from '../AcademicFramework';
import ImageSlider from '../Slider/ImageSlider';
import Footer from '../Footer';
import Parallax from '../Parallex';
// import Hallone from '../Hall/Seminarone';
// import Parallax from '../Parallex';
// import Hallone from '../Hall/Seminarone';
// import Facility from '../facility/facitlity';
import Hallone from '../Hall/Seminarone';
import Facility from '../facility/facitlity';
import Hero from '../Hero';
import Climate from '../Climate'


const Layout = () => {
  return (
    <>
      {/* <Parallax /> */}
      <Hero />
      {/* <News /> */}
      <Navigation />
      <Climate />
      {/* <Hallone /> */}
      <Institute />
      <AcademicFramework />
      <ImageSlider />
      {/* <SquareAnimation /> */}
      <Footer />
    </>
  );
}

export default Layout;
