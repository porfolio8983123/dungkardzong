import React from 'react';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import SldImg1 from '../Img/1.png';
import SldImg2 from '../Img/2.png';
import SldImg3 from '../Img/3.png';
import SldImg4 from '../Img/4.png';
import SldImg5 from '../Img/5.png';
import SldImg6 from '../Img/Lib/p56.jpg';
import SldImg7 from '../Img/7.png';
import SldImg8 from '../Img/18.png';
import SldImg9 from '../Img/19.png';
import SldImg10 from '../Img/GameHall/p49.jpg';
import SldImg11 from '../Img/11.png';
import SldImg12 from '../Img/12.png';
import SldImg13 from '../Img/13.png';


import { useNavigate } from 'react-router-dom';

import './ImageSlider.scss';

const ViewDetailsButton = ({id}) => {

  const navigate = useNavigate();

  return (
    <div className="view-details-button">
      <button className="details-button"
        onClick={() => {
          navigate(`/details/${id}`);
        }}
      >More Details</button>
    </div>
  );
};

const ImageSlider = () => {
  const settings = {
    dots: false,
    infinite: false,
    speed: 500,
    slidesToShow: 2,
    slidesToScroll: 1,
    arrows: true,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
        },
      },
    ],
  };

  const slideData = [
    {
      id: 1,
      img: SldImg7,
      title: "Meditation Center",
      description: "A place for meditation and mindfulness practices.",
      capacity: "Seating Capcapacity : 60",
    },
    {
      id: 2,
      img: SldImg8,
      title: "High-Altitude Athletics Training",
      description: "Training facility for high-altitude athletics.",
      capacity: "Seating Capcapacity : 3000",
    },
    {
      id: 3,
      img: SldImg1,
      title: "Seminar Hall 1",
      description: "High-end venue for prestigious events and corporate conferences. It is equipped with advanced technology, luxurious furnishings, and top-tier audio-visual systems.",
      capacity: "Seating Capacity: 60",
    },
    {
      id: 4,
      img: SldImg2,
      title: "Seminar Hall 2",
      description: "Venue for the faculty of DGI to host seminars, conferences, workshops, lectures, and various academic or professional events.",
      capacity: "Seating Capacity: 94"
    },
    {
      id: 5,
      img: SldImg3,
      title: "Seminar Hall 3",
      description: "Dedicated venue for students to host presentations, workshops, and other academic events equipped with foldable seating pads.",
      capacity: "Seating Capacity: 102"
    },    
    {
      id: 6,
      img: SldImg4,
      title: "Conference Hall",
      description: "Space for a variety of meetings involving a broader group of participants.",
      capacity: "Seating Capacity: 33(Max 80)"
    },
    {
      id: 7,
      img: SldImg5,
      title: "Board Room",
      description: "For executive-level meetings.",
      capacity: "Seating Capacity: 22(Max 50)"
    },
    {
      id: 8,
      img: SldImg6,
      title: "Library",
      description: "Library with a collection of books and resources.",
      capacity: "Seating Capacity : 60",
    },
    {
      id: 9,
      img: SldImg9,
      title: "Founders Court",
      description: "The Founder's Court is a versatile and elegant covered courtyard.",
      capacity: "Seating Capacity : 192",
    },
    {
      id: 10,
      img: SldImg10,
      title: "Multi-Purpose Hall",
      description: "Discover the ultimate venue for indoor sports, fitness, and wellness events.",
      capacity: "Seating Capacity : 1300",
    },
    {
      id: 11,
      img: SldImg11,
      title: "Lounge Room",
      description: "Discover the perfect venue for smaller, more intimate events.",
      capacity: "Seating Capacity : 60",
    },
    {
      id: 12,
      img: SldImg12,
      title: "Bilateral Room",
      description: "Bilateral RoomElevate your business meetings and negotiations with our Bilateral Meeting Room, designed to provide a focused and professional environment.",
      capacity: "Seating Capacity : 20",
    },
    {
      id: 13,
      img: SldImg13,
      title: "Dining Hall",
      description: "Our Dining Hall is a magnificent space designed to host large-scale dining events, offering an elegant and spacious environment for your guests.",
      capacity: "Seating Capacity : 800",
    }
  ];

  return (
    <div className="slider-container" id = "facilityContainer" style={{zIndex:"999"}}>
      <Slider {...settings}>
        {slideData.map((slide, index) => (
          <div key={index} className="slide">
            <img src={slide.img} alt={`Facility ${index + 1}`} className="slider-image" />
            <div className="slide-overlay">
              <div className="slide-content">
                <h3 className="slide-title">{slide.title}</h3>
                <p className="slide-description">{slide.description}</p>
                <p className="slide-capacity">{slide.capacity}</p>
              </div>
              <ViewDetailsButton id = {slide.id} />
            </div>
          </div>
        ))}
      </Slider>
    </div>
  );
};

const SampleNextArrow = (props) => {
  const { className, style, onClick } = props;
  return (
    <div
      className={className}
      style={{ ...style, display: "block", right: "0px" }}
      onClick={onClick}
    />
  );
};

const SamplePrevArrow = (props) => {
  const { className, style, onClick } = props;
  return (
    <div
      className={className}
      style={{ ...style, display: "block", left: "0px", zIndex: 1 }}
      onClick={onClick}
    />
  );
};

export default ImageSlider;
