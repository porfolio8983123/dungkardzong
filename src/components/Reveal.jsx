import { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import compass from "../assets/compass.svg";
import logo from "../assets/logo.svg";
import videoSource from '../assets/Dzong Video Street View.webm';
import "../Styles/Reveal.scss";
import LoadingAnimation from './LoadingAnimation';

import Sky from '../assets/sky.webp';
import Wintersky from '../assets/winter/sky.webp';
import Springsky from '../assets/spring/sky.webp';
import Summersky from '../assets/summer/sky.webp';
import Autumnsky from '../assets/autumn/sky.webp';

import mountainBG from '../assets/bgmountain.svg';
import WintermountainBG from '../assets/winter/bgmountain.webp';
import SpringmountainBG from '../assets/spring/bgmountain.webp';
import SummermountainBG from '../assets/summer/bgmountain.svg';
import AutumnmountainBG from '../assets/autumn/bgmountain.svg';

import mountainFG from '../assets/mountainFG.svg';
import WintermountainFG from '../assets/winter/mountainFG.svg';
import SpringmountainFG from '../assets/spring/mountainFG.svg';
import SummermountainFG from '../assets/summer/mountainFG.svg';
import AutumnmountainFG from '../assets/autumn/mountainFG.svg';

import mountainFG2 from '../assets/mountaifg2.svg';
import WintermountainFG2 from '../assets/winter/mountaifg2.svg';
import SpringmountainFG2 from '../assets/spring/mountaifg2.svg';
import SummermountainFG2 from '../assets/summer/mountaifg2.svg';
import AutumnmountainFG2 from '../assets/autumn/mountaifg2.svg';

import Dzong from '../assets/MainFG.webp';
import Winterdzong from '../assets/winter/MainFG.webp';
import Springdzong from '../assets/spring/MainFG.webp';
import Summerdzong from '../assets/summer/MainFG.webp';
import Autumndzong from '../assets/autumn/MainFG.webp';

const Reveal = ({ children,onLoadComplete }) => {
  const [isVideoRevealed, setIsVideoRevealed] = useState(false);
  const [isPageLoaded, setIsPageLoaded] = useState(false);
  const navigate = useNavigate();
  
  // const texts = ["Our Heavenly", "PANGBISA:", "Modern Aspirations Meet", "Ugyen Guru Lhakhang", "Druk Gyalpo's Institute", "Royal Academy:"];
  // const texts2 = ["DUNGKAR", "Where legends intertwine", "ancient history", "Sacred sanctuary", "beacon of knowledge", "Tradition meets innovation"];
  const texts = [""];
  const texts2 = [""];
  const [currentText, setCurrentText] = useState(texts[0]);
  const [currentText2, setCurrentText2] = useState(texts2[0]);
  const [index, setIndex] = useState(0);

  const [imagesLoaded, setImagesLoaded] = useState(false);

  useEffect(() => {
    const preloadImages = async () => {
      const images = [
        Sky,
        Wintersky, 
        Springsky, 
        Summersky, 
        Autumnsky,
        Dzong,
        Winterdzong,
        Springdzong,
        Summerdzong, 
        Autumndzong, 
        mountainBG,
        WintermountainBG,
        SpringmountainBG,
        SummermountainBG,
        AutumnmountainBG,
        mountainFG,
        WintermountainFG,
        SummermountainFG,
        SpringmountainFG,
        AutumnmountainFG,
        mountainFG2,
        WintermountainFG2,
        SpringmountainFG2,
        SummermountainFG2,
        AutumnmountainFG2
      ];
      await Promise.all(images.map((image) => new Promise((resolve) => {
        const img = new Image();
        img.src = image;
        img.onload = resolve;
      })));
      setImagesLoaded(true);
      onLoadComplete();
    };

    preloadImages();
  }, [onLoadComplete]);

  useEffect(() => {
    const handleLoad = () => {
      setIsPageLoaded(true);
      setIsVideoRevealed(true);
      onLoadComplete && onLoadComplete();
    };

    window.addEventListener('load', handleLoad);
    return () => window.removeEventListener('load', handleLoad);
  }, [onLoadComplete]);

  useEffect(() => {
    const changeText = () => {
      setIndex(prevIndex => (prevIndex + 1) % texts.length);
    };

    const timer = setInterval(changeText, 5000); // Change text every 5 seconds

    return () => clearInterval(timer);
  }, [texts.length]);

  useEffect(() => {
    setCurrentText(texts[index]);
    setCurrentText2(texts2[index]);
  }, [index, texts, texts2]);

  const handleExploreDungkarClick = () => {
    setIsVideoRevealed(false);
    setTimeout(() => {
      setIsPageLoaded(false);
      setTimeout(() => navigate("/home"), 5000); // Navigate after clouds have moved in
    }, 5000); // Slight delay to allow the cloud move-in animation to start
  };

  return (
    <div className="reveal">
       {!imagesLoaded && <LoadingAnimation />}
       {imagesLoaded && children}
      <div className={`clouds ${isPageLoaded ? 'move-out' : ''} ${!isVideoRevealed ? 'move-in' : ''}`}>
        <div className="cloud top-left"></div>
        <div className="cloud top-right"></div>
        <div className="cloud bottom-left"></div>
        <div className="cloud bottom-right"></div>
      </div>
      <div className={`video-container ${isVideoRevealed ? 'revealed' : ''}`}>
        <video autoPlay muted loop className="video">
          <source src={videoSource} />
          Your browser does not support HTML5 video.
        </video>
        <div className="overlayReveal">
          <img className='logo' src={logo} alt="Logo" />
          <div className='text'>
            <h2>{currentText}</h2>
            <h1>{currentText2}</h1>
          </div>
          <div className="explore-button-wrapper">
            <div className="Buttoncloud"></div>
            <div className='explore-button-container'>
              <button className="explore-button" onClick={handleExploreDungkarClick}>Explore Dungkar</button>
              <img className='compass' src={compass} alt="Compass" onClick={handleExploreDungkarClick}/>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Reveal;
