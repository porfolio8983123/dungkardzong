import React, { useState } from "react";
import Polygon from "react-polygon";

const MyPolygon = () => {
  const [points] = useState([
    [25, 25],
    [75, 25],
    [100, 75],
    [75, 125],
    [25, 125],
    [0, 75],
  ]);

  return (
    <div align="center">
      <div>
        <Polygon n={6} size={200} points={points} />
        <Polygon n={6} size={200} points={points} />
      </div>
      <div>
        <Polygon n={6} size={200} points={points} />
        <Polygon n={6} size={200} points={points} />
        <Polygon n={6} size={200} points={points} />
      </div>
      <div>
        <Polygon n={6} size={200} points={points} />
        <Polygon n={6} size={200} points={points} />
      </div>
      {/* ... repeat for other rows */}
    </div>
  );
};

export default MyPolygon;
