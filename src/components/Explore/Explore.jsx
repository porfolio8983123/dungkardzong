import React, { useEffect, useState, useRef } from 'react';
import './Explore.scss';
import exploreImage from '../Img/Explore.png'; // Update the path as necessary
import image1 from '../Img/image1.png';
import image2 from '../Img/image2.png';
import image3 from '../Img/image3.png';
import image4 from '../Img/image4.png';
import image5 from '../Img/image5.png';
import image6 from '../Img/image6.png';
import centerImage from '../../assets/logo/logo1.png'; // Newly added central image
import { motion } from 'framer-motion';

const Explore = ({settingExplore}) => {
  const centerImageRef = useRef(null); // Ref for the center image

  const polygons = [
    { id: 1, name: "Panoramic view of Pangbisa village.", image: image1,description: "Pangbisa, nestled at 2500-3000 meters above sea level, is a serene village just a half-hour drive from Paro International Airport. With approximately 25 households, this village is renowned for its historical and spiritual significance." },
    { id: 2, name: "Ugyen Guru Lhakhang", image: image2, description: "The sacred Ugyen Guru Lhakhang crowns the ridge, overlooking the valley. This ancient temple, founded by Terton Sherab Mebar, houses sacred relics and stands as a testament to the village's deep spiritual roots." },
    { id: 3, name: "Terton Sherab Mebar", image: image3, description: "Terton Sherab Mebar (1267-1326), the revered treasure revealer, founded Ugyen Guru Lhakhang. His expeditions across Bhutan left a legacy of sacred relics and spiritual teachings." },
    { id: 4, name: "Legends and Myths", image: image4, description: "Legends say Zhabdrung Ngawang Namgyel visited Pangbisa in the 16th century, seeking followers. Unwelcomed by the devoted locals, he named it 'Abandoned Land' and moved to the neighboring village." },
    { id: 5, name: "Spiritual Guardians", image: image5, description: "Protected by the Four Tsens and blessed by the Five Long Life Sisters, Pangbisa is a spiritual sanctuary. These guardians and deities are believed to watch over the village, ensuring its prosperity and spiritual harmony." },
    { id: 6, name: "The Druk Gyalpo’s Institute", image: image6, description: "His Majesty Jigme Khesar Namgyel Wangchuck envisioned The Druk Gyalpo’s Institute as a beacon of holistic education. Spanning 285 acres, the Institute bridges ancient wisdom with modern educational practices." }
  ];

  const angleIncrement = 360 / polygons.length;

  const [exploring, setExploring] = useState(false);
  const [centerImagePosition, setCenterImagePosition] = useState({ x: 0, y: 0 });
  const [previousImagePosition, setPreviousImagePosition] = useState({ x: 0, y: 0 });
  const [hoveredHexagon, setHoveredHexagon] = useState(null);

  useEffect(() => {
    if (centerImageRef.current) {
      const { x, y } = centerImageRef.current.getBoundingClientRect();
      setPreviousImagePosition({ x, y });
      setCenterImagePosition({ x, y });
    }
  }, []);

  useEffect(() => {
    if (centerImageRef.current) {
      const { x, y } = centerImageRef.current.getBoundingClientRect();
      setPreviousImagePosition(centerImagePosition);
      setCenterImagePosition({ x, y });
    }
  }, [exploring]);

  useEffect(() => {
    console.log("position ", centerImagePosition);
    console.log("previous ", previousImagePosition);
  }, [centerImagePosition]);

  return (
    <div className='ourStory__main__container'>
      <motion.div
        className='ourStory__container'
        initial={{ x: 0 }}
        animate={{ x: exploring ? centerImagePosition.x - 600 : 0 }}
        transition={{ duration: 0.9, ease: "easeInOut" }}
        style={{ height: exploring ? "100vh" : "80vh", marginTop: exploring ? "220px" : 0 }}
      >
        <div className='hexagons'>
          {polygons.map((item, index) => {
            const angleRad = (index * angleIncrement * Math.PI) / 180;
            const radius = !exploring ? 190 : 400;
            const x = radius * Math.cos(angleRad) + 'px';
            const y = radius * Math.sin(angleRad) + 'px';

            return (
              <div
                className='hexagon'
                key={index}
                onMouseEnter={() => setHoveredHexagon(index)}
                onMouseLeave={() => setHoveredHexagon(null)}
                style={{
                  backgroundImage: `url(${item.image})`,
                  position: 'absolute',
                  transform: `translate(${x}, ${y}) rotate(-30deg) scale(${exploring ? 2 : 1})`,
                  transition: 'transform 0.9s ease-in-out', // Smooth transition for scale
                }}
              >
                {exploring && hoveredHexagon === index && (
                  <div className="hexagon__overlay">
                    <div style={{width:"70%"}}>
                      <p style={{ color: 'white', zIndex:"999",fontSize:"10px", marginBottom:"10px",textAlign:"center" }}>{index+1}. {item.name}</p>
                      <p style={{ color: 'white', zIndex:"999",fontSize:"8px", textAlign:"center"}}>{item.description}</p>
                    </div>
                  </div>
                )}
              </div>
            );
          })}
          <div
            className='hexagon center-hexagon'
            ref={centerImageRef}
            style={{
              position: 'absolute',
              transform: 'translate(-50%, -50%) rotate(-30deg)',
              left: '50%',
              top: '50%',
              width: '200px',
              height: '200px',
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
              flexDirection: 'column',
              textAlign: 'center',
            }}
          >
            <img src={centerImage} alt="Center Logo" style={{ width: exploring ? "150px" : '60px', height: 'auto' }} />
            <p
              style={{
                margin: '10px 0 0 0',
                fontSize: '24px',
                color: '#FE3600',
                fontWeight: 'bold',
                textDecoration: "underline",
                cursor: "pointer"
              }}
              onClick={() => {
                console.log("hello shit");
                setExploring(!exploring);
                settingExplore();
              }}
            >
              {exploring ? "Close" : "Explore"}
            </p>
          </div>
        </div>
      </motion.div>

      {!exploring &&
        <div style={{ height: "80vh", display: "flex", alignItems: "center", width: "40%" }}>
          <div>
            <p style={{ fontSize: "96px", color: "#FE3600", fontWeight: "bold" }}>How It All</p>
            <p style={{ fontSize: "96px", color: "#FE3600", fontWeight: "bold" }}>Started?</p>
          </div>
        </div>
      }
    </div>
  );
};

export default Explore;
