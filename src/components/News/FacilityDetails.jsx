import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import Header from '../Header';
import Footer from '../Footer';
import './news.scss'; // Import the SCSS file
import { RotatingSquare } from 'react-loader-spinner';

const FacilityDetails = () => {
    const { id } = useParams();
    const [currentFacility, setCurrentFacility] = useState({});
    const [morePhotos, setMorePhotos] = useState([]);
    const [loading, setLoading] = useState(false); // Add loading state

    const handleGetNews = async () => {
        await fetch("https://dungkarserver.onrender.com/api/v1/currentNews/getOneFacility", {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({ id: id })
        })
            .then(async (response) => {
                const responseData = await response.json();
                console.log("news data ", responseData);

                if (responseData.status === 'success') {
                    setCurrentFacility(responseData.data);
                }
            })
            .catch((error) => {
                console.log("error ", error);
            });
    };

    const getMorePhotos = async () => {
        setLoading(true); // Start loading
        await fetch("https://dungkarserver.onrender.com/api/v1/currentNews/getMorePhotos", {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({ id: id })
        })
            .then(async (response) => {
                const responseData = await response.json();
                console.log("news data ", responseData);

                if (responseData.status === 'success') {
                    setMorePhotos(responseData.data.morePhotos);
                    console.log("response data more photos ", responseData.data);
                }
            })
            .catch((error) => {
                console.log("error ", error);
            })
            .finally(() => {
                setLoading(false); // End loading
            });
    };

    useEffect(() => {
        handleGetNews();
    }, [id]);

    useEffect(() => {
        getMorePhotos();
    }, [id]);

    useEffect(() => {
        console.log("current facility ", currentFacility);
    }, [currentFacility]);

    return (
        <>
            <Header />
            <div className="news-banner">
                <h1>News & Events</h1>
            </div>
            <div className="facility-details-container">
                <div className="facility-cover">
                    <img src={currentFacility.coverPhoto} alt={currentFacility.title} className="facility-photo" />
                    <div className="facility-overlay">
                        <h2>{currentFacility.title}</h2>
                    </div>
                </div>
                <p className="facility-description">{currentFacility.description}</p>
                <p className="facility-description">Date of Event: {new Date(currentFacility.date).toLocaleDateString()}</p>
                <p className="facility-description">Room: {currentFacility.room}</p>
                <h3>Images</h3>
                <div className="facility-images">
                    {loading ? (
                        <div className="loader">
                            <RotatingSquare
                                height="100"
                                width="100"
                                color="#4fa94d"
                                ariaLabel="rotating-square-loading"
                                strokeWidth="4"
                                visible={true}
                            />
                        </div>
                    ) : (
                        morePhotos.length > 0 && morePhotos.map((image, index) => (
                            <img key={index} src={image} alt={`${currentFacility.title} ${index + 1}`} className="facility-image" />
                        ))
                    )}
                </div>
            </div>
            <Footer />
        </>
    );
};

export default FacilityDetails;
