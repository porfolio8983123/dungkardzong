import React, { useState } from 'react';
import facebook from './Img/facebook-logo.png';
import instagram from './Img/instagram-logo.png';
import { useNavigate } from 'react-router-dom';

const Footer = () => {

    const navigate = useNavigate();

    const [formData, setFormData] = useState({ email: '' });
    const [isValidEmail, setIsValidEmail] = useState(true);
    const [responseError,setResponseError] = useState(false);
    const [responseMessage,setResponseMessage] = useState('');
    const [success,setSuccess] = useState(false);

    const handleChange = (e) => {
        const { name, value } = e.target;
        setFormData({ ...formData, [name]: value });
        validateEmail(value);
        setResponseError(false);
    };

    const validateEmail = (email) => {
        const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
        setIsValidEmail(emailRegex.test(email));
    };

    const inputStyle = {
        border: `1px solid ${formData.email ? '#FE3600' : 'black'}`,
        color: `${formData.email ? '#FE3600' : 'black'}`,
        padding: '10px',
        marginBottom: '8px',
        width: '100%',
        fontSize: '12px',
        fontFamily: 'Bricolage Grotesque',
        borderRadius: '5px',
        boxSizing: 'border-box',
        textAlign: 'center',
    };

    const buttonStyle = {
        backgroundColor: '#FE3600',
        color: 'white',
        padding: '10px',
        width: '100%',
        fontSize: '12px',
        fontFamily: 'Bricolage Grotesque',
        border: 'none',
        borderRadius: '5px',
        boxSizing: 'border-box',
        cursor: isValidEmail ? 'pointer' : 'not-allowed',
        opacity: isValidEmail ? 1 : 0.5,
        transition: 'background-color 0.3s ease', // Smooth transition for background color
    };

    const footerStyle = {
        backgroundColor: '#412E27',
        padding: '20px 40px',
        display: 'flex',
        justifyContent: 'space-between',
        flexWrap: 'wrap',
        zIndex: "999"
    };

    const sectionStyle = {
        padding: '10px',
    };

    const contactUsStyle = {
        display: 'flex',
        flexDirection: 'row',
        color: 'white',
    };

    const addressStyle = {
        fontSize: '12px',
        flexDirection: 'column',
    };

    const mapStyle = {
        border: '0',
    };

    const footerTextStyle = {
        textAlign: 'center',
        color: 'white',
        width: '100%',
        marginTop: '20px',
        fontSize: '12px',
    };

    return (
        <footer style={footerStyle}>
            <div style={sectionStyle}>
                <h4 style={{ color: '#FE3600', fontSize: '18px', padding: '5px', }}>Quick Links</h4>
                <div>
                    <p style={{ color: 'white', fontSize: '12px', padding: '5px',cursor:'pointer' }}
                        onClick={() => {
                            navigate("/home");
                        }}
                    >Home</p>
                    <p style={{ color: 'white', fontSize: '12px', padding: '5px',cursor:'pointer' }}
                        onClick={() => {
                            navigate("/ourStory")
                        }}
                    >Our Stories</p>
                    <p style={{ color: 'white', fontSize: '12px', padding: '5px',cursor:"pointer" }}
                        onClick={() => {
                            navigate("/facilities")
                        }}
                    >Our Facilities</p>
                    <p style={{ color: 'white', fontSize: '12px', padding: '5px',cursor:"pointer" }}
                        onClick={() => {
                            navigate("/news")
                        }}
                    >News and Events</p>
                </div>
            </div>

            <div style={sectionStyle}>
                <h4 style={{ color: '#FE3600', fontSize: '18px', padding: '5px' }}>Address & Contact</h4>
                <div style={contactUsStyle} className="contact-us">
                    <div style={addressStyle}>
                        <p style={{ padding: '5px' }}>Dungkar Dzong Pangbisa, Paro,</p>
                        <p style={{ padding: '5px' }}>Bhutan, PO Box 11001</p>
                        <p style={{ padding: '5px' }}>02 333000</p>
                    </div>
                </div>
            </div>

            <div style={sectionStyle}>
                <h4 style={{ color: '#FE3600', fontSize: '18px', padding: '5px' }}>Social Media</h4>
                <div style={{ paddingLeft: '13px' }}>
                    <img src={instagram} alt="Instagram" style={{ padding: '5px' }} />
                    <img src={facebook} alt="Facebook" style={{ padding: '5px' }} />
                </div>
            </div>

            <div style={sectionStyle}>
                <h4 style={{ color: '#FE3600', fontSize: '18px', padding: '5px' }}>Subscribe to our Newsletter</h4>
                <div>
                    <input
                        type="email"
                        name="email"
                        placeholder="Email"
                        value={formData.email}
                        onChange={handleChange}
                        style={inputStyle}
                    />
                    <button id='subscribe' style={buttonStyle} disabled={!isValidEmail}
                        onClick={async () => {
                            await fetch("http://localhost:4001/api/v1/news/addNewsLetter", {
                                method:"POST",
                                headers: {
                                    "Content-Type":"application/json"
                                },
                                body: JSON.stringify(formData)
                            })
                            .then(async (response) => {
                                const responseData = await response.json();
                                console.log("response ", responseData);
                                if (responseData.status === 'error') {
                                    setSuccess(false)
                                    setResponseMessage(responseData.error)
                                    setResponseError(true);
                                } else {
                                    setResponseError(false);
                                    setSuccess(true);
                                }
                            })
                            .catch((error) => {
                                console.log(error);
                                setSuccess(false);
                                setResponseError(true);
                            })
                        }}
                    >Subscribe</button>
                    {!isValidEmail && (
                        <p style={{ color: 'red', fontSize: '12px', textAlign: 'center' }}>Please enter a valid email address.</p>
                    )}

                    {responseError && (
                        <p style={{ color: 'red', fontSize: '12px', textAlign: 'center' }}>{responseMessage}</p>
                    )}

                    {success && (
                        <p style={{ color: 'green', fontSize: '12px', textAlign: 'center' }}>Email Added successfully!</p>
                    )}
                </div>
            </div>

            <div style={sectionStyle}>
                <h4 style={{ color: '#FE3600', fontSize: '18px', padding: '5px' }}>Our Location</h4>
                <iframe
                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2506.2269029721656!2d89.44368956613823!3d27.333834636165463!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39e18373092d64cf%3A0x693560e4bff3ccdc!2sROYAL%20ACADEMY!5e0!3m2!1sen!2sbt!4v1718408803520!5m2!1sen!2sbt"
                    width="300"
                    height="150"
                    style={mapStyle}
                    allowFullScreen=""
                    loading="lazy"
                    title="Location Map"
                ></iframe>
            </div>

            <div style={footerTextStyle}>
                All rights reserved &copy; 2024 Dungkar Dzong/Enki Realm
            </div>

            <style>
                {`
                    #subscribe:hover {
                        background-color: #d12d00 !important;
                    }

                    @media (max-width: 480px) {
                        footer {
                            flex-direction: column;
                            align-items: center;
                            padding: 20px;
                        }
                        h4 {
                            text-align: center;
                        }
                        p {
                            text-size: 10px;
                        }
                        .row {
                            display: flex;
                            width: 100%;
                            justify-content: space-around;
                        }
                        .column {
                            width: 100%;
                            display: flex;
                            justify-content: center;
                        }
                        iframe {
                            width: 100%;
                            height: 200px;
                        }
                    }
                `}
            </style>
        </footer>
    );
};

export default Footer;
