import React, {useRef, useEffect, useState} from 'react';
import * as THREE from 'three';
import { TweenLite } from 'gsap';
import hm3 from '../../assets/aboutImages/HM3.jpg';
import img1 from '../../assets/aboutImages/DJI_0279_80_81_82_83_Enhancer.jpg';
import img2 from '../../assets/aboutImages/DJI_0279_80_81_82_83_Enhancer.jpg';
import img3 from '../../assets/aboutImages/DSC_1932.jpg';

const DisplacementSlider = () => {

    const sliderRef = useRef(null);
    const [images, setImages] = useState([]);
    const [currentSlide, setCurrentSlide] = useState(0); // State to manage current slide index

    useEffect(() => {
        // Add global gtag script
        const gtagScript = document.createElement('script');
        gtagScript.src = "https://www.googletagmanager.com/gtag/js?id=UA-40525870-5";
        gtagScript.async = true;
        document.body.appendChild(gtagScript);
        
        gtagScript.onload = () => {
          window.dataLayer = window.dataLayer || [];
          window.gtag = function(){window.dataLayer.push(arguments);}
          window.gtag('js', new Date());
          window.gtag('config', 'UA-40525870-5');
        };
    
        // Set up Three.js and handle slider functionality
        const initSlider = () => {
          if (!sliderRef.current) return;
    
          const sliderEl = sliderRef.current;
          const images = Array.from(sliderEl.querySelectorAll('img'));
          const sliderImages = [];
          const canvasWidth = images[0].clientWidth;
          const canvasHeight = images[0].clientHeight;
          const renderWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
          const renderHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
    
          let renderW, renderH;
    
          if (renderWidth > canvasWidth) {
            renderW = renderWidth;
          } else {
            renderW = canvasWidth;
          }
    
          renderH = canvasHeight;
    
          const renderer = new THREE.WebGLRenderer({
            antialias: false,
          });
    
          renderer.setPixelRatio(window.devicePixelRatio);
          renderer.setClearColor(0x23272A, 1.0);
          renderer.setSize(renderW, renderH);
          sliderEl.appendChild(renderer.domElement);
    
          const loader = new THREE.TextureLoader();
          loader.crossOrigin = "anonymous";
    
          images.forEach((img) => {
            const image = loader.load(img.getAttribute('src') + '?v=' + Date.now());
            image.magFilter = image.minFilter = THREE.LinearFilter;
            image.anisotropy = renderer.capabilities.getMaxAnisotropy();
            sliderImages.push(image);
          });
    
          const scene = new THREE.Scene();
          scene.background = new THREE.Color(0x23272A);
          const camera = new THREE.OrthographicCamera(
            renderWidth / -2,
            renderWidth / 2,
            renderHeight / 2,
            renderHeight / -2,
            1,
            1000
          );
    
          camera.position.z = 1;
    
          const mat = new THREE.ShaderMaterial({
            uniforms: {
              dispFactor: { type: "f", value: 0.0 },
              currentImage: { type: "t", value: sliderImages[0] },
              nextImage: { type: "t", value: sliderImages[1] },
            },
            vertexShader: `
              varying vec2 vUv;
              void main() {
                vUv = uv;
                gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
              }
            `,
            fragmentShader: `
              varying vec2 vUv;
              uniform sampler2D currentImage;
              uniform sampler2D nextImage;
              uniform float dispFactor;
              void main() {
                vec2 uv = vUv;
                vec4 orig1 = texture2D(currentImage, uv);
                vec4 orig2 = texture2D(nextImage, uv);
                vec4 _currentImage = texture2D(currentImage, vec2(uv.x, uv.y + dispFactor * (orig2 * 0.3)));
                vec4 _nextImage = texture2D(nextImage, vec2(uv.x, uv.y + (1.0 - dispFactor) * (orig1 * 0.3)));
                vec4 finalTexture = mix(_currentImage, _nextImage, dispFactor);
                gl_FragColor = finalTexture;
              }
            `,
            transparent: true,
            opacity: 1.0
          });
    
          const geometry = new THREE.PlaneGeometry(
            sliderEl.offsetWidth,
            sliderEl.offsetHeight,
            1
          );
          const object = new THREE.Mesh(geometry, mat);
          object.position.set(0, 0, 0);
          scene.add(object);
    
          const addEvents = () => {
            const pagButtons = Array.from(document.getElementById('pagination').querySelectorAll('button'));
            let isAnimating = false;
    
            pagButtons.forEach((el) => {
              el.addEventListener('click', function() {
                if (!isAnimating) {
                  isAnimating = true;
    
                  document.getElementById('pagination').querySelectorAll('.active')[0].className = '';
                  this.className = 'active';
    
                  const slideId = parseInt(this.dataset.slide, 10);
    
                  mat.uniforms.nextImage.value = sliderImages[slideId];
                  mat.uniforms.nextImage.needsUpdate = true;
    
                  TweenLite.to(mat.uniforms.dispFactor, 1, {
                    value: 1,
                    ease: 'Expo.easeInOut',
                    onComplete: function () {
                      mat.uniforms.currentImage.value = sliderImages[slideId];
                      mat.uniforms.currentImage.needsUpdate = true;
                      mat.uniforms.dispFactor.value = 0.0;
                      isAnimating = false;
                    }
                  });
    
                  const slideTitleEl = document.getElementById('slide-title');
                  const slideStatusEl = document.getElementById('slide-status');
    
                  const titleElements = document.querySelectorAll(`[data-slide-title="${slideId}"]`);
                  const statusElements = document.querySelectorAll(`[data-slide-status="${slideId}"]`);
    
                  if (titleElements.length > 0) {
                    const nextSlideTitle = titleElements[0].innerHTML;
    
                    TweenLite.fromTo(slideTitleEl, 0.5,
                      { autoAlpha: 1, y: 0 },
                      { autoAlpha: 0, y: 20, ease: 'Expo.easeIn', onComplete: function () {
                          slideTitleEl.innerHTML = nextSlideTitle;
                          TweenLite.to(slideTitleEl, 0.5, { autoAlpha: 1, y: 0 });
                        }
                      });
                  }
    
                  if (statusElements.length > 0) {
                    const nextSlideStatus = statusElements[0].innerHTML;
    
                    TweenLite.fromTo(slideStatusEl, 0.5,
                      { autoAlpha: 1, y: 0 },
                      { autoAlpha: 0, y: 20, ease: 'Expo.easeIn', onComplete: function () {
                          slideStatusEl.innerHTML = nextSlideStatus;
                          TweenLite.to(slideStatusEl, 0.5, { autoAlpha: 1, y: 0 });
                        }
                      });
                  }
                }
              });
            });
          };
    
          addEvents();
    
          const animate = () => {
            requestAnimationFrame(animate);
            renderer.render(scene, camera);
          };
    
          animate();
        };
    
        initSlider();
    
        // Automatic slide transition
        const interval = setInterval(() => {
          setCurrentSlide(prevSlide => {
            const nextSlide = (prevSlide + 1) % 4; // assuming 4 slides
            const slideButtons = document.querySelectorAll('#pagination button');
            slideButtons.forEach(button => button.classList.remove('active'));
            slideButtons[nextSlide].classList.add('active');
    
            // Trigger click event on the corresponding pagination button
            slideButtons[nextSlide].click();
            return nextSlide;
          });
        }, 5000); // Change slide every 5 seconds
    
        return () => {
          clearInterval(interval);
          // Cleanup Three.js and event listeners if necessary
        };
      }, []);


    return (
        <main>
            <div id="slider" ref={sliderRef}>
                <div className="slider-inner">
                <div id="slider-content">
                    <div className="meta">Title</div>
                    <h2 id="slide-title">The Genesis of The Royal Vision</h2>
                    <span data-slide-title="0">The Genesis of The Royal Vision</span>
                    <span data-slide-title="1">The Triad of Excellence at Druk Gyalpo’s Institute</span>
                    <span data-slide-title="2">A Symbol of Cultural and Environmental Commitment</span>
                    <span data-slide-title="3">The Modern Educational Paradigm</span>
                </div>
                </div>
                <img src={hm3} alt="His Majesty's Portrait1" />
                <img src={img1} alt="His Majesty's Portrait2" />
                <img src={img2} alt="His Majesty's Portrait3" />
                <img src={img3} alt="His Majesty's Portrait4" />
                <div id="pagination">
                <button className="active" data-slide="0"></button>
                <button data-slide="1"></button>
                <button data-slide="2"></button>
                <button data-slide="3"></button>
                </div>
            </div>
        </main>
    )
}

export default DisplacementSlider