import React from 'react';
import DisplacementSlider from './DisplacementSlider';
import img4 from '../../assets/aboutImages/HM3.jpg';
import img5 from '../../assets/aboutImages/img2.png';
import img6 from '../../assets/aboutImages/img3.png';
import './style.css';

const About = () => {
  return (
    <div>
        <DisplacementSlider />
        <div id="about-us-info">
        <div className='about-us-content'>
          <h2>The Genesis of The Royal Vision</h2>
          <div className='about-us-content-row'>
            <img src={img4} alt="His Majesty's Portrait for aboutus" />
            <p>In a profound reflection of The Royal Vision, the majestic Gesarling embodies the rich cultural heritage and forward-thinking educational vision of Bhutan. Twelve years ago, His Majesty the King, during a royal tour, envisioned the future prominence of Gesarling. This vision seamlessly integrated three noble aspirations: to fabricate a Dzong, dissipate the 21st Century Education System, and the community's aspiration for an educational center. This initiative not only honors tradition but also heralds a new era of academic excellence and systems thinking.</p>
          </div>
        </div>
        <div className='about-us-content'>
          <h2>The Triad of Excellence at Druk Gyalpo’s Institute</h2>
          <div className='about-us-content-row' id='hide-in-phone'>
            <p>At the heart of Druk Gyalpo’s Institute are three pivotal centers: The Royal Academy, the Education Research Center, and the Teacher Development Center. These centers collaboratively strive towards comprehensive individual development across five key dimensions: Emotional, Cerebral, Spiritual, Physical, and Social. This integrated approach ensures a well-rounded and profound educational experience, nurturing future leaders who are equipped to thrive in an ever-evolving world.</p>
            <img src={img5} alt="His Majesty's Portrait for aboutus" />
          </div>
          <div className='about-us-content-row' id='view-in-phone'>
          <img src={img5} alt="His Majesty's Portrait for aboutus" />
            <p>At the heart of Druk Gyalpo’s Institute are three pivotal centers: The Royal Academy, the Education Research Center, and the Teacher Development Center. These centers collaboratively strive towards comprehensive individual development across five key dimensions: Emotional, Cerebral, Spiritual, Physical, and Social. This integrated approach ensures a well-rounded and profound educational experience, nurturing future leaders who are equipped to thrive in an ever-evolving world.</p>
          </div>
        </div>
        <div className='about-us-content'>
          <h2>A Symbol of Cultural and Environmental Commitment</h2>
          <div className='about-us-content-row'>
            <img src={img6} alt="His Majesty's Portrait for aboutus" />
            <p>Nestled in a nation celebrated for its carbon negativity and adherence to the principles of Gross National Happiness, the site of Gesarling is a testament to Bhutan's rich cultural heritage. Here, the promise of future advancements in education and architectural magnificence converge, embodying both historical reverence and contemporary ambition.</p>
          </div>
        </div>
        <div className='about-us-content'>
          <h2>The Modern Educational Paradigm</h2>
          <div className='about-us-content-row' id='hide-in-phone'>
            <p>Today, Gesarling stands as a beacon of modern education and systems thinking, embodied in the esteemed Druk Gyalpo’s Institute. This institute fosters a holistic approach to learning, emphasizing not only theoretical knowledge but also the cultivation of virtuous and capable human beings. The curriculum is designed to develop novel skills, instill the right mindset, and promote both cerebral and physical growth among young leaders.
            Gesarling and Druk Gyalpo’s Institute epitomize the harmonious blend of tradition and innovation, setting a new benchmark for holistic education in the 21st century.</p>
            <img src={img4} alt="His Majesty's Portrait for aboutus" />
          </div>
          <div className='about-us-content-row' id='view-in-phone'>
          <img src={img4} alt="His Majesty's Portrait for aboutus" />
            <p>Today, Gesarling stands as a beacon of modern education and systems thinking, embodied in the esteemed Druk Gyalpo’s Institute. This institute fosters a holistic approach to learning, emphasizing not only theoretical knowledge but also the cultivation of virtuous and capable human beings. The curriculum is designed to develop novel skills, instill the right mindset, and promote both cerebral and physical growth among young leaders.
            Gesarling and Druk Gyalpo’s Institute epitomize the harmonious blend of tradition and innovation, setting a new benchmark for holistic education in the 21st century.</p>
            
          </div>
        </div>
        <div className='about-us-content'>
          <h2>The state-of-the-art  facilities with cutting-edge designs</h2>
          <div className='about-us-content-row'>
          <img src={img5} alt="His Majesty's Portrait for aboutus" />
            <p>The Gesarling is equipped with the modern facilities and stands as a beacon of contemporary elegance and technological sophistication. Designed to accommodate a variety of events, from professional conferences to educational seminars and corporate meetings, this facility is a testament to architectural innovation and functional design.
Nestled within a state-of-the-art building, the modern conference hall exudes contemporary elegance and technological sophistication, accommodating events from professional conferences to corporate meetings. The spacious, well-lit interior blends modern aesthetics with practical functionality, fostering a productive atmosphere.
Equipped with a cutting-edge central heating system, the hall maintains optimal temperature year-round. High-definition projectors, large LED screens, and an integrated sound system provide impeccable audio-visual experiences, while wireless microphones and conferencing tools ensure seamless communication.
Ergonomic chairs and adjustable tables prioritize comfort and support, catering to various seating arrangements. Additional amenities include high-speed Wi-Fi and strategically placed charging stations, ensuring all technological needs are met effortlessly. This hall offers a versatile, comfortable environment for all events.</p>
          </div>
        </div>
      </div>
    </div>
  )
}

export default About