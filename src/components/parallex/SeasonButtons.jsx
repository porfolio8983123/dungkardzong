import React from 'react';
import { motion } from "framer-motion";
import WinterSmall from "../svg/WinterSmall";
import SpringSmall from "../svg/SpringSmall";
import SummerSmall from "../svg/SummerSmall";
import AutumnSmall from "../svg/AutumnSmall";

export default function SeasonButtons({ handleImageChange, activeButton, textColor, buttonDivRef }) {
  return (
    <motion.div ref={buttonDivRef} style={{ display: 'flex', justifyContent: 'space-around', width: "100% !important" }}>
      <motion.button 
        onClick={() => handleImageChange("Winter")}
        animate={{ backgroundColor: activeButton === "Winter" ? "#FFFFFF" : "transparent", color: activeButton === "Winter" ? textColor : "#FFFFFF" }}
        transition={{ duration: 0.5 }}>
        <div style={{ display: "flex" }}>
          <div><WinterSmall color={activeButton === "Winter" ? textColor : "#FFFFFF"} /></div>
          <div>Winter</div>
        </div>
      </motion.button>
      <motion.button 
        onClick={() => handleImageChange("Spring")} 
        animate={{ color: activeButton === "Spring" ? textColor : "#FFFFFF", backgroundColor: activeButton === "Spring" ? "#FFFFFF" : "transparent" }}
        transition={{ duration: 0.5 }}>
        <div style={{ display: "flex" }}>
          <div><SpringSmall color={activeButton === "Spring" ? textColor : "#FFFFFF"} /></div>
          <div>Spring</div>
        </div>
      </motion.button>
      <motion.button 
        onClick={() => handleImageChange("Summer")} 
        animate={{ color: activeButton === "Summer" ? textColor : "#FFFFFF", backgroundColor: activeButton === "Summer" ? "#FFFFFF" : "transparent" }}
        transition={{ duration: 0.5 }}>
        <div style={{ display: "flex" }}>
          <div><SummerSmall color={activeButton === "Summer" ? textColor : "#FFFFFF"} /></div>
          <div>Summer</div>
        </div>
      </motion.button>
      <motion.button 
        onClick={() => handleImageChange("Autumn")} 
        animate={{ color: activeButton === "Autumn" ? textColor : "#FFFFFF", backgroundColor: activeButton === "Autumn" ? "#FFFFFF" : "transparent" }}
        transition={{ duration: 0.5 }}>
        <div style={{ display: "flex" }}>
          <div><AutumnSmall color={activeButton === "Autumn" ? textColor : "#FFFFFF"} /></div>
          <div>Autumn</div>
        </div>
      </motion.button>
    </motion.div>
  );
}
