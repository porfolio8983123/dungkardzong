import React from 'react';
import sunmoon from "../../assets/sun&moon.svg";

export default function InfoCards({
  cardColor, seasonBig, monthText, tempText, lowestTemp,
  lowestTempText, precipitation, humidity, wind,
  precipitationText, humidityText, windText,
  sun, sunriseText, sunsetText, moon,
  moonriseText, moonsetText
}) {
  return (
    <div style={{ display: 'flex', justifyContent: 'space-evenly', width: '70%', marginTop: '5%' }}>
      <div className="card" style={{ backgroundColor: cardColor, display: "flex", alignItems: "center" }}>
        <div className="content" style={{ alignItems: "center" }}>
          <div><img src={seasonBig} alt="seasonBig"/></div>
          <div><h1>{monthText}</h1></div>
        </div>
      </div>
      <div className="card" style={{ backgroundColor: cardColor, display: "flex", alignItems: "center" }}>
        <div className="content" style={{ margin: "5%", alignItems: "flex-start" }}>
          <h1 style={{ fontSize: "4rem", fontWeight: "800" }}>{tempText}</h1>
          <p>{lowestTemp}</p>
          <p>{lowestTempText}</p>
        </div>
        <div className="content" style={{ display: "flex", flexDirection: "column", padding: "0.8em" }}>
          <p className="bold">{precipitationText}</p>
          <p>{precipitation}</p>
          <p className="bold">{humidityText}</p>
          <p>{humidity}</p>
          <p className="bold">{windText}</p>
          <p>{wind}</p>
        </div>
      </div>
      <div className="card" style={{ backgroundColor: cardColor, display: "flex" }}>
        <div className="content" style={{ margin: "5%" }}>
          <div><img src={sunmoon} alt="sunmoon"/></div>
        </div>
        <div className="content" style={{ height: "100%" }}>
          <div style={{ padding: "0.8em", height: "100%" }}>
            <div style={{ margin: "5px", height: "50%" }}>
              <p className="bold">{sun}</p>
              <p style={{ whiteSpace: "nowrap" }}>{sunriseText}</p>
              <p>{sunsetText}</p>
            </div>
            <div style={{ margin: "5px", height: "50%" }}>
              <p className="bold">{moon}</p>
              <p>{moonriseText}</p>
              <p>{moonsetText}</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
