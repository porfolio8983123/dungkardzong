import logo from "../../assets/logo.svg";

export default function LogoTitle({ seasonText, textColor, titleRef }) {
  return (
    <>
      <img src={logo} alt="Logo" className="logo" />
      <h1 className="subTitle" style={{ color: textColor }}>{seasonText}</h1>
      <h1 className="Title" ref={titleRef}>DUNGKAR DZONG</h1>
    </>
  );
}
