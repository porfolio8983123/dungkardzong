import { motion, useScroll, useTransform } from "framer-motion";
import { useRef, useState, useEffect } from "react";
import { useMediaQuery } from 'react-responsive';

import LoadingAnimation from "../LoadingAnimation";
import "../../Styles/Parallex.scss";

import Sky from '../../assets/sky.svg';
import Wintersky from '../../assets/winter/sky.svg';
import Springsky from '../../assets/spring/sky.svg';
import Summersky from '../../assets/summer/sky.svg';
import Autumnsky from '../../assets/autumn/sky.svg';

import mountainBG from '../../assets/bgmountain.svg';
import WintermountainBG from '../../assets/winter/bgmountain.svg';
import SpringmountainBG from '../../assets/spring/bgmountain.svg';
import SummermountainBG from '../../assets/summer/bgmountain.svg';
import AutumnmountainBG from '../../assets/autumn/bgmountain.svg';

import mountainFG from '../../assets/mountainFG.svg';
import WintermountainFG from '../../assets/winter/mountainFG.svg';
import SpringmountainFG from '../../assets/spring/mountainFG.svg';
import SummermountainFG from '../../assets/summer/mountainFG.svg';
import AutumnmountainFG from '../../assets/autumn/mountainFG.svg';

import mountainFG2 from '../../assets/mountaifg2.svg';
import WintermountainFG2 from '../../assets/winter/mountaifg2.svg';
import SpringmountainFG2 from '../../assets/spring/mountaifg2.svg';
import SummermountainFG2 from '../../assets/summer/mountaifg2.svg';
import AutumnmountainFG2 from '../../assets/autumn/mountaifg2.svg';

import Dzong from '../../assets/MainFG.svg';
import Winterdzong from '../../assets/winter/MainFG.svg';
import Springdzong from '../../assets/spring/MainFG.svg';
import Summerdzong from '../../assets/summer/MainFG.svg';
import Autumndzong from '../../assets/autumn/MainFG.svg';

import winterBig from '../../assets/winter/winterBig.svg';
import springBig from '../../assets/spring/springBig.svg';
import summerBig from '../../assets/summer/summerBig.svg';
import autumnBig from '../../assets/autumn/autumnBig.svg';

import LogoTitle from './LogoTitle';
import SeasonButtons from './SeasonButtons';
import InfoCards from './InfoCards';

export default function Parallax() {
  const ref = useRef(null);
  const titleRef = useRef(null);
  const [isLoading, setIsLoading] = useState(true);
  const [isVisible, setIsVisible] = useState(false);
  const [activeButton, setActiveButton] = useState("Winter");

  const isTabletOrMobile = useMediaQuery({ query: '(max-width: 1224px)' });
  const buttonDivRef = useRef(null);

  const handleImageChange = (season) => {
    setActiveButton(season);
    setIsLoading(true);
  };

  useEffect(() => {
    const timer = setTimeout(() => {
      setIsLoading(false);
    }, 1000);

    const observer = new IntersectionObserver((entries) => {
      entries.forEach((entry) => {
        if (entry.isIntersecting) {
          setIsVisible(true);
          observer.unobserve(entry.target);
        }
      });
    }, { threshold: 0.5 });

    if (titleRef.current) {
      observer.observe(titleRef.current);
    }

    return () => {
      clearTimeout(timer);
      if (titleRef.current) {
        observer.unobserve(titleRef.current);
      }
    };
  }, [activeButton]);

  useEffect(() => {
    const handleScroll = () => {
      if (buttonDivRef.current) {
        const { bottom } = buttonDivRef.current.getBoundingClientRect();
        if (bottom < 0) {
          buttonDivRef.current.style.display = "none";
        } else {
          buttonDivRef.current.style.display = "flex";
        }
      }
    };
    window.addEventListener("scroll", handleScroll);
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  const seasonImages = {
    Winter: { sky: Wintersky, bg: WintermountainBG, fg: WintermountainFG, fg2: WintermountainFG2, dzong: Winterdzong, color: "#ADD8E6", text: "Winter", cardColor: "#E6E6FA", seasonBig: winterBig },
    Spring: { sky: Springsky, bg: SpringmountainBG, fg: SpringmountainFG, fg2: SpringmountainFG2, dzong: Springdzong, color: "#98FB98", text: "Spring", cardColor: "#F0E68C", seasonBig: springBig },
    Summer: { sky: Summersky, bg: SummermountainBG, fg: SummermountainFG, fg2: SummermountainFG2, dzong: Summerdzong, color: "#FFD700", text: "Summer", cardColor: "#FFA07A", seasonBig: summerBig },
    Autumn: { sky: Autumnsky, bg: AutumnmountainBG, fg: AutumnmountainFG, fg2: AutumnmountainFG2, dzong: Autumndzong, color: "#FF8C00", text: "Autumn", cardColor: "#D2691E", seasonBig: autumnBig }
  };

  const { sky, bg, fg, fg2, dzong, color, text, cardColor, seasonBig } = seasonImages[activeButton];

  const { scrollYProgress } = useScroll();
  const y1 = useTransform(scrollYProgress, [0, 1], [0, 600]);
  const y2 = useTransform(scrollYProgress, [0, 1], [0, 400]);
  const y3 = useTransform(scrollYProgress, [0, 1], [0, 300]);
  const y4 = useTransform(scrollYProgress, [0, 1], [0, 200]);

  return (
    <div className="Main">
      {!isLoading && (
        <motion.div className="Parallax" ref={ref}>
          <motion.img src={sky} alt="Sky" style={{ y: y1 }} />
          <motion.img src={bg} alt="Background" style={{ y: y2 }} />
          <motion.img src={fg} alt="Foreground" style={{ y: y3 }} />
          <motion.img src={fg2} alt="Foreground 2" style={{ y: y4 }} />
          <motion.img src={dzong} alt="Dzong" />
          <div className="Text" style={{ opacity: isVisible ? 1 : 0, transition: "opacity 1s" }}>
            <LogoTitle seasonText={text} textColor={color} titleRef={titleRef} />
            <SeasonButtons
              handleImageChange={handleImageChange}
              activeButton={activeButton}
              textColor={color}
              buttonDivRef={buttonDivRef}
            />
            <InfoCards
              cardColor={cardColor}
              seasonBig={seasonBig}
              monthText="Month Info"
              tempText="Temp Info"
              lowestTemp="Lowest Temp"
              lowestTempText="Lowest Temp Text"
              precipitation="Precipitation"
              humidity="Humidity"
              wind="Wind"
              precipitationText="Precipitation Info"
              humidityText="Humidity Info"
              windText="Wind Info"
              sun="Sun Info"
              sunriseText="Sunrise"
              sunsetText="Sunset"
              moon="Moon Info"
              moonriseText="Moonrise"
              moonsetText="Moonset"
            />
          </div>
        </motion.div>
      )}
    </div>
  );
}
