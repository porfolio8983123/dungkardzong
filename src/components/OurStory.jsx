import React, { useEffect, useState } from 'react';
import Header from './Header';
import '../Styles/ourStory.scss';
import Footer from '../components/Footer';
import { useNavigate } from 'react-router-dom';
import { Circles } from 'react-loader-spinner';

const OurStory = () => {
    const navigate = useNavigate();
    const [events, setEvents] = useState([]);
    const [loading, setLoading] = useState(true);

    const handleGetNews = async () => {
        setLoading(true); // Start loading
        await fetch("https://dungkarserver.onrender.com/api/v1/currentNews/getAllNews")
            .then(async (response) => {
                const responseData = await response.json();
                console.log("news data ", responseData);

                if (responseData.status === 'success') {
                    setEvents(responseData.data);
                }
                setLoading(false); // Stop loading
            })
            .catch((error) => {
                console.log("error ", error);
                setLoading(false); // Stop loading even on error
            });
    };

    useEffect(() => {
        handleGetNews();
    }, []);

    useEffect(() => {
        console.log("events ", events);
    }, [events]);

    const formatDate = (dateStr) => {
        const date = new Date(dateStr);
        return date.toLocaleDateString('en-US', { day: 'numeric', month: 'long', year: 'numeric' });
    };

    return (
        <div>
            <Header />
            <div className='event__container1'>
                <p className='event__title1'>News & Events</p>
                <div className='news1'>
                    <div className='event__innercontainer1'>
                        {loading ? (
                            <div className="loader-container" style={{height:"400px",display:'flex',justifyContent: 'center', alignItems:'center'}}>
                                <Circles
                                    height="80"
                                    width="80"
                                    color="#4fa94d"
                                    ariaLabel="circles-loading"
                                    wrapperStyle={{}}
                                    wrapperClass=""
                                    visible={true}
                                    />
                            </div>
                        ) : (
                            events.length > 0 && events.map((item, index) => (
                                <div className='card1' key={index}>
                                    <img src={item.coverPhoto} alt="" />
                                    <div className='event__description__container1'
                                        style={{ cursor: "pointer" }}
                                        onClick={() => {
                                            navigate(`/newsEvents/${item._id}`)
                                        }}
                                    >
                                        <p className='title1'>{item.title}</p>
                                        <p className='sub1'>{formatDate(item.date)}</p>
                                    </div>
                                </div>
                            ))
                        )}
                    </div>
                </div>
            </div>
            <Footer />
        </div>
    )
}

export default OurStory;
