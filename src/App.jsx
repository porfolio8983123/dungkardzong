import { lazy, Suspense,useState,useEffect } from 'react';
import { BrowserRouter as Router, Routes, Route} from 'react-router-dom';
import Reveal from './components/Reveal'; 
const Layout = lazy(() => import('./components/layout/Layout'));
// const Reveal = lazy(() => import('./components/Reveal'));
const OurStory = lazy(() => import('./components/OurStory'));
const Story  = lazy(() => import('./components/Story'));
const Facility = lazy(() => import('./components/facility/facitlity'));
const Hallone = lazy(() => import('./components/Hall/Seminarone'));
const FacilityDetails = lazy(() => import('./components/News/FacilityDetails'));


function App() {
  const [isLoading, setIsLoading] = useState(true);

  const handleLoadComplete = () => {
    setIsLoading(false);
  };

  return (
   <>
    <Router>
    {/* {isLoading && <Reveal onLoadComplete={handleLoadComplete} />} */}
    {/* <Suspense fallback={<Reveal onLoadComplete={handleLoadComplete} />}> */}
    <Suspense fallback={null} >
      <Routes>
        {/* <Route path = "/" element = {<Reveal />} /> */}
        <Route path = "/" element = {<Layout />} />
        <Route path = "/news" element = {<OurStory />} />
        <Route path = "/ourStory" element = {<Story />} />
        <Route path = "/facilities" element = {<Facility />} />
        <Route path = "/details/:id" element = {<Hallone />} />
        <Route path="/newsEvents/:id" element={<FacilityDetails />} />
      </Routes>
      </Suspense>
    </Router>
   </>
  )
}

export default App
