import { configureStore } from "@reduxjs/toolkit";
import { eventReducer } from "./slices/eventSlice";
import { fetchEventsStart,fetchEventsFailure,fetchEventsSuccess } from "./slices/eventSlice";

export const store = configureStore({
    reducer: {
        event: eventReducer
    }
})

export {
    fetchEventsFailure,
    fetchEventsStart,
    fetchEventsSuccess
}