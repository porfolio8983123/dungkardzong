import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'

// https://vitejs.dev/config/
export default defineConfig({
  server:{
    watch: {
      usePolling:true,
    },
  },
  plugins: [react()],
  build: {
    outDir: 'dist', // Default output directory for Vite is 'dist'
  },
  assetsInclude: ['**/*.JPG']
})
